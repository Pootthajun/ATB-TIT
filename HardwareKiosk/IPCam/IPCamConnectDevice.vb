﻿Imports System.Windows.Forms
Imports IPCam.Org.Mentalis.Files

Public Class IPCamConnectDevice

    Public INIFileName As String = Application.StartupPath & "\ConfigDevice.ini"
    Dim CS As New IPCamClass

    Private Sub txtVID_KeyUp(sender As Object, e As KeyEventArgs) Handles txtVID.KeyUp
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SETTING"
        ini.Write("IPCamVID", txtVID.Text)
        Connect()
    End Sub

    Sub Connect()
        Try
            If CS.ConnectDevice Then
                txtStatus.Text = "Ready"
                txtStatus.ForeColor = Drawing.Color.Lime
                Exit Sub
            End If
        Catch ex As Exception : End Try
        txtStatus.Text = "Disconnected"
        txtStatus.ForeColor = Drawing.Color.Red
    End Sub

    Private Sub IPCamConnectDevice_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SETTING"
        txtVID.Text = ini.ReadString("IPCamVID")
        txtVID.Enabled = True
        Connect()
    End Sub


End Class
