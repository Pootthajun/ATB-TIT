﻿Imports IPCam.Org.Mentalis.Files
Imports System.Windows.Forms
Imports USBClassLibrary

Public Class IPCamClass

    Public INIFileName As String = Application.StartupPath & "\ConfigDevice.ini"

    Public Function ConnectDevice()
        Try
            Dim ini As New IniReader(INIFileName)
            ini.Section = "SETTING"
            Dim VID As String = ini.ReadString("IPCamVID")
            Dim USBPort As New USBClass
            Dim ListOfUSBDeviceProperties As New List(Of USBClass.DeviceProperties)()
            Dim MyUSBDeviceConnected As Boolean = False
            If USBClass.GetUSBDevice(VID, "", ListOfUSBDeviceProperties, True) = True Then
                Return True
            End If
        Catch ex As Exception : End Try
        Return False
    End Function
End Class
