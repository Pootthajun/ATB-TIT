﻿Imports NetworkConnection.Org.Mentalis.Files

Public Class Connection


    Dim IPAddress As String
    Dim NWC As New NetworkConnectionClass
    Public Property ConnectIP() As String
        Get
            Return IPAddress
        End Get
        Set(ByVal value As String)
            IPAddress = value
            lblHead.Text = "IP Address " & IPAddress
        End Set
    End Property

    Private Sub Connection_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim ini As New IniReader(NWC.INIFileName)
        ini.Section = "SETTING"
        ini.Write("NetworkConnection", IPAddress)

        If NWC.CheckStatus = NWC.NetworkConnectionStatus.Ready Then
            txtStatus.Text = "Ready"
            txtStatus.ForeColor = Drawing.Color.Lime
        Else
            txtStatus.Text = "Disconnected"
            txtStatus.ForeColor = Drawing.Color.Red
        End If
    End Sub
End Class
