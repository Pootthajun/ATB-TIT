﻿Imports NetworkConnection.Org.Mentalis.Files

Public Class NetworkConnectionClass

    Public INIFileName As String = Application.StartupPath & "\ConfigDevice.ini"

    Public Enum NetworkConnectionStatus
        Ready = 1
        Disconnected = 2
    End Enum

    Function CheckStatus() As NetworkConnectionStatus
        Try
            Dim ini As New IniReader(INIFileName)
            ini.Section = "SETTING"
            Dim IPAddress As String = ini.ReadString("NetworkConnection")

            If My.Computer.Network.Ping(IPAddress) Then
                Return NetworkConnectionStatus.Ready
            End If
        Catch ex As Exception : End Try
        Return NetworkConnectionStatus.Disconnected
    End Function
End Class
