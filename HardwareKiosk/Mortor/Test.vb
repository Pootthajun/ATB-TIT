﻿Imports Mortor.Org.Mentalis.Files

Public Class Test

    Dim Mortor As New MotorClass
    Dim MotorIDValue As Integer

    Public Property MotorID() As Integer
        Get
            Return MotorIDValue
        End Get
        Set(ByVal value As Integer)
            MotorIDValue = value
        End Set
    End Property

    Sub BindComport()
        cbbComport.Items.Clear()
        cbbComport.Items.Add("")
        For Each sp As String In My.Computer.Ports.SerialPortNames
            cbbComport.Items.Add(sp)
        Next
    End Sub

    Sub Connect()
        If Mortor.ConnectDevice(MotorIDValue) = True Then
            AddHandler Mortor.MySerialPort.DataReceived, AddressOf Mortor.MySerialPortDataReceived
            AddHandler Mortor.ReceiveEvent, AddressOf DataReceived
            BindpbStatus(True)
        Else
            BindpbStatus(False)
        End If
    End Sub

    Private Sub DataReceived(ByVal ReceiveData As String)
        If ReceiveData = "" Then Exit Sub
        TextBox1.Text = ReceiveData
    End Sub

    Private Sub cbbComport_SelectionChangeCommitted(sender As System.Object, e As System.EventArgs) Handles cbbComport.SelectionChangeCommitted
        Dim ini As New IniReader(Mortor.INIFileName)
        ini.Section = "SETTING"
        ini.Write("Mortor" & MotorIDValue & "Comport", cbbComport.Text)
        Connect()
    End Sub

    Sub BindpbStatus(ByVal Status As Boolean)
        If Status = True Then
            lblStatus.Text = "Connected"
            lblStatus.ForeColor = Drawing.Color.Lime
        Else
            lblStatus.Text = "Disconnected"
            lblStatus.ForeColor = Drawing.Color.Red
        End If
    End Sub

    Private Sub Test_Load(sender As Object, e As EventArgs) Handles Me.Load
        CheckForIllegalCrossThreadCalls = False
        BindComport()
        Connect()
        Mortor.BindPin(cbbLeft)
        Mortor.BindPin(cbbRight)
    End Sub

    Private Sub btnLeftStart_Click(sender As Object, e As EventArgs) Handles btnLeftStart.Click
        Mortor.RollLeftStart(MotorIDValue)
    End Sub

    Private Sub btnLeftStop_Click(sender As Object, e As EventArgs) Handles btnLeftStop.Click
        Mortor.RollLeftStop(MotorIDValue)
    End Sub

    Private Sub btnRightStart_Click(sender As Object, e As EventArgs) Handles btnRightStart.Click
        Mortor.RollRightStart(MotorIDValue)
    End Sub

    Private Sub btnRightStop_Click(sender As Object, e As EventArgs) Handles btnRightStop.Click
        Mortor.RollRightStop(MotorIDValue)
    End Sub

    Private Sub cbbLeft_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cbbLeft.SelectionChangeCommitted
        Dim ini As New IniReader(Mortor.INIFileName)
        ini.Section = "SETTING"
        ini.Write("Mortor" & MotorIDValue & "PinLeft", cbbLeft.SelectedValue.ToString)
    End Sub

    Private Sub cbbRight_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cbbRight.SelectionChangeCommitted
        Dim ini As New IniReader(Mortor.INIFileName)
        ini.Section = "SETTING"
        ini.Write("Mortor" & MotorIDValue & "PinRight", cbbLeft.SelectedValue.ToString)
    End Sub

    Private Sub cbbComport_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbComport.SelectedIndexChanged

    End Sub
End Class
