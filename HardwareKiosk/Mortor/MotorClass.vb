﻿Imports System.IO.Ports
Imports System.Windows.Forms
Imports Mortor.Org.Mentalis.Files
Imports Converter
Imports System.Threading

Public Class MotorClass

    Public MySerialPort As New SerialPort
    Public INIFileName As String = Application.StartupPath & "\ConfigDevice.ini"

    Public Enum Mortor_Command
        Start_Mortor = 1
        Stop_Mortor = 2
    End Enum

    Public Function ConnectDevice() As Boolean
        Try
            Dim ini As New IniReader(INIFileName)
            ini.Section = "SETTING"
            If MySerialPort.IsOpen Then MySerialPort.Close()
            MySerialPort.PortName = ini.ReadString("MortorComport")
            MySerialPort.BaudRate = 9600
            MySerialPort.Parity = Parity.None
            MySerialPort.StopBits = StopBits.One
            MySerialPort.DataBits = 8
            MySerialPort.Handshake = Handshake.None
            'MySerialPort.ReadTimeout = 1000
            MySerialPort.Open()
            Return True
        Catch ex As Exception
            MySerialPort.Close()
            Return False
        End Try
    End Function

    Public Function ConnectDevice(ByVal Comport As String) As Boolean
        Try
            If MySerialPort.IsOpen Then MySerialPort.Close()
            MySerialPort.PortName = Comport
            MySerialPort.BaudRate = 9600
            MySerialPort.Parity = Parity.None
            MySerialPort.StopBits = StopBits.One
            MySerialPort.DataBits = 8
            MySerialPort.Handshake = Handshake.None
            MySerialPort.Open()
            Return True
        Catch ex As Exception
            MySerialPort.Close()
            Return False
        End Try
    End Function

    Public Sub Disconnect()
        ConnectDevice("")
    End Sub

    Public Sub BindPin(ByRef cbb As ComboBox)
        cbb.Items.Clear()
        cbb.DisplayMember = "Text"
        cbb.ValueMember = "Value"
        Dim dt As New DataTable
        dt.Columns.Add("Text", GetType(String))
        dt.Columns.Add("Value", GetType(Integer))
        dt.Rows.Add("--- Select ---", 0)
        dt.Rows.Add("Pin 2", Integer.Parse("2", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin 3", Integer.Parse("3", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin 4", Integer.Parse("4", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin 5", Integer.Parse("5", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin 6", Integer.Parse("6", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin 7", Integer.Parse("7", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin 8", Integer.Parse("8", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin 9", Integer.Parse("9", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin 10", Integer.Parse("A", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin 11", Integer.Parse("B", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin 12", Integer.Parse("C", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin 13", Integer.Parse("D", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin A0", Integer.Parse("E", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin A1", Integer.Parse("F", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin A2", Integer.Parse("10", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin A3", Integer.Parse("11", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin A4", Integer.Parse("12", System.Globalization.NumberStyles.HexNumber))
        dt.Rows.Add("Pin A5", Integer.Parse("13", System.Globalization.NumberStyles.HexNumber))
        cbb.DataSource = dt
        cbb.SelectedIndex = 0


    End Sub

    Public Sub RollRightStart(ByVal DeviceID As Int32)
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SETTING"
        Dim Pin As String = ini.ReadString("DSPR" & DeviceID)
        Dim data As Byte() = {Pin, Mortor_Command.Start_Mortor}
        If Not MySerialPort.IsOpen Then MySerialPort.Open()
        MySerialPort.Write(data, 0, data.Length)
    End Sub

    Public Sub RollRightStop(ByVal DeviceID As Int32)
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SETTING"
        Dim Pin As String = ini.ReadString("DSPR" & DeviceID)
        Dim data As Byte() = {Pin, Mortor_Command.Stop_Mortor}
        If Not MySerialPort.IsOpen Then MySerialPort.Open()
        MySerialPort.Write(data, 0, data.Length)
    End Sub

    Public Sub RollLeftStart(ByVal DeviceID As Int32)
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SETTING"
        Dim Pin As String = ini.ReadString("DSPL" & DeviceID)
        Dim data As Byte() = {Pin, Mortor_Command.Start_Mortor}
        If Not MySerialPort.IsOpen Then MySerialPort.Open()
        MySerialPort.Write(data, 0, data.Length)
    End Sub

    Public Sub RollLeftStop(ByVal DeviceID As Int32)
        Dim ini As New IniReader(INIFileName)
        ini.Section = "SETTING"
        Dim Pin As String = ini.ReadString("DSPL" & DeviceID)
        Dim data As Byte() = {Pin, Mortor_Command.Stop_Mortor}
        If Not MySerialPort.IsOpen Then MySerialPort.Open()
        MySerialPort.Write(data, 0, data.Length)
    End Sub

    Public Event ReceiveEvent(ByVal ReceiveData As String)
    Public Sub MySerialPortDataReceived()
        Thread.Sleep(100)
        Dim Hex As String = ""
        For i As Integer = 0 To MySerialPort.BytesToRead - 1
            Dim d As Integer = MySerialPort.ReadByte
            Dim b(0) As Byte
            b(0) = d
            Hex &= BytesToHexString(b) & " "
        Next
        RaiseEvent ReceiveEvent(Hex)
    End Sub

End Class
