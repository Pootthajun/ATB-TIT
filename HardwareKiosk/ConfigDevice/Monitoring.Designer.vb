﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Monitoring
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.ScanPassportConnectDevice1 = New ScanPassport.ScanPassportConnectDevice()
        Me.CashInConnectDevice1 = New CashIn.CashInConnectDevice()
        Me.CoinInConnectDevice1 = New CoinIn.CoinInConnectDevice()
        Me.CashOutConnectDevice1 = New CashOut.CashOutConnectDevice()
        Me.CashOutConnectDevice2 = New CashOut.CashOutConnectDevice()
        Me.CoinOutConnectDevice1 = New CoinOut.CoinOutConnectDevice()
        Me.PrinterConnectDevice1 = New Printer.PrinterConnectDevice()
        Me.BarcodeScannerConnectDavice1 = New BarcodeScanner.BarcodeScannerConnectDavice()
        Me.ScanPassportConnectDevice2 = New ScanPassport.ScanPassportConnectDevice()
        Me.IpCamConnectDevice1 = New IPCam.IPCamConnectDevice()
        Me.SuspendLayout()
        '
        'ScanPassportConnectDevice1
        '
        Me.ScanPassportConnectDevice1.BackColor = System.Drawing.Color.White
        Me.ScanPassportConnectDevice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ScanPassportConnectDevice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ScanPassportConnectDevice1.Location = New System.Drawing.Point(648, 2)
        Me.ScanPassportConnectDevice1.Margin = New System.Windows.Forms.Padding(4)
        Me.ScanPassportConnectDevice1.Name = "ScanPassportConnectDevice1"
        Me.ScanPassportConnectDevice1.Size = New System.Drawing.Size(320, 101)
        Me.ScanPassportConnectDevice1.TabIndex = 6
        '
        'CashInConnectDevice1
        '
        Me.CashInConnectDevice1.BackColor = System.Drawing.Color.White
        Me.CashInConnectDevice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CashInConnectDevice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CashInConnectDevice1.Location = New System.Drawing.Point(3, 3)
        Me.CashInConnectDevice1.Margin = New System.Windows.Forms.Padding(4)
        Me.CashInConnectDevice1.Name = "CashInConnectDevice1"
        Me.CashInConnectDevice1.Size = New System.Drawing.Size(320, 160)
        Me.CashInConnectDevice1.TabIndex = 0
        '
        'CoinInConnectDevice1
        '
        Me.CoinInConnectDevice1.BackColor = System.Drawing.Color.White
        Me.CoinInConnectDevice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CoinInConnectDevice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CoinInConnectDevice1.Location = New System.Drawing.Point(3, 166)
        Me.CoinInConnectDevice1.Margin = New System.Windows.Forms.Padding(4)
        Me.CoinInConnectDevice1.Name = "CoinInConnectDevice1"
        Me.CoinInConnectDevice1.Size = New System.Drawing.Size(320, 160)
        Me.CoinInConnectDevice1.TabIndex = 1
        '
        'CashOutConnectDevice1
        '
        Me.CashOutConnectDevice1.BackColor = System.Drawing.Color.White
        Me.CashOutConnectDevice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CashOutConnectDevice1.CashID = 18
        Me.CashOutConnectDevice1.CashType = 50
        Me.CashOutConnectDevice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CashOutConnectDevice1.Location = New System.Drawing.Point(326, 3)
        Me.CashOutConnectDevice1.Margin = New System.Windows.Forms.Padding(4)
        Me.CashOutConnectDevice1.Name = "CashOutConnectDevice1"
        Me.CashOutConnectDevice1.Size = New System.Drawing.Size(320, 160)
        Me.CashOutConnectDevice1.TabIndex = 2
        '
        'CashOutConnectDevice2
        '
        Me.CashOutConnectDevice2.BackColor = System.Drawing.Color.White
        Me.CashOutConnectDevice2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CashOutConnectDevice2.CashID = 3
        Me.CashOutConnectDevice2.CashType = 100
        Me.CashOutConnectDevice2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CashOutConnectDevice2.Location = New System.Drawing.Point(649, 3)
        Me.CashOutConnectDevice2.Margin = New System.Windows.Forms.Padding(4)
        Me.CashOutConnectDevice2.Name = "CashOutConnectDevice2"
        Me.CashOutConnectDevice2.Size = New System.Drawing.Size(320, 160)
        Me.CashOutConnectDevice2.TabIndex = 3
        '
        'CoinOutConnectDevice1
        '
        Me.CoinOutConnectDevice1.BackColor = System.Drawing.Color.White
        Me.CoinOutConnectDevice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CoinOutConnectDevice1.CoinID = 5
        Me.CoinOutConnectDevice1.CoinType = 1
        Me.CoinOutConnectDevice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CoinOutConnectDevice1.Location = New System.Drawing.Point(326, 166)
        Me.CoinOutConnectDevice1.Margin = New System.Windows.Forms.Padding(4)
        Me.CoinOutConnectDevice1.Name = "CoinOutConnectDevice1"
        Me.CoinOutConnectDevice1.Size = New System.Drawing.Size(320, 160)
        Me.CoinOutConnectDevice1.TabIndex = 4
        '
        'PrinterConnectDevice1
        '
        Me.PrinterConnectDevice1.BackColor = System.Drawing.Color.White
        Me.PrinterConnectDevice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PrinterConnectDevice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.PrinterConnectDevice1.Location = New System.Drawing.Point(972, 3)
        Me.PrinterConnectDevice1.Margin = New System.Windows.Forms.Padding(4)
        Me.PrinterConnectDevice1.Name = "PrinterConnectDevice1"
        Me.PrinterConnectDevice1.Size = New System.Drawing.Size(320, 99)
        Me.PrinterConnectDevice1.TabIndex = 5
        '
        'BarcodeScannerConnectDavice1
        '
        Me.BarcodeScannerConnectDavice1.BackColor = System.Drawing.Color.White
        Me.BarcodeScannerConnectDavice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BarcodeScannerConnectDavice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.BarcodeScannerConnectDavice1.Location = New System.Drawing.Point(972, 104)
        Me.BarcodeScannerConnectDavice1.Margin = New System.Windows.Forms.Padding(4)
        Me.BarcodeScannerConnectDavice1.Name = "BarcodeScannerConnectDavice1"
        Me.BarcodeScannerConnectDavice1.Size = New System.Drawing.Size(320, 96)
        Me.BarcodeScannerConnectDavice1.TabIndex = 6
        '
        'ScanPassportConnectDevice2
        '
        Me.ScanPassportConnectDevice2.BackColor = System.Drawing.Color.White
        Me.ScanPassportConnectDevice2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ScanPassportConnectDevice2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ScanPassportConnectDevice2.Location = New System.Drawing.Point(649, 166)
        Me.ScanPassportConnectDevice2.Margin = New System.Windows.Forms.Padding(4)
        Me.ScanPassportConnectDevice2.Name = "ScanPassportConnectDevice2"
        Me.ScanPassportConnectDevice2.Size = New System.Drawing.Size(320, 133)
        Me.ScanPassportConnectDevice2.TabIndex = 7
        '
        'IpCamConnectDevice1
        '
        Me.IpCamConnectDevice1.BackColor = System.Drawing.Color.White
        Me.IpCamConnectDevice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.IpCamConnectDevice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.IpCamConnectDevice1.Location = New System.Drawing.Point(972, 203)
        Me.IpCamConnectDevice1.Margin = New System.Windows.Forms.Padding(4)
        Me.IpCamConnectDevice1.Name = "IpCamConnectDevice1"
        Me.IpCamConnectDevice1.Size = New System.Drawing.Size(320, 96)
        Me.IpCamConnectDevice1.TabIndex = 8
        '
        'Monitoring
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1084, 775)
        Me.Controls.Add(Me.IpCamConnectDevice1)
        Me.Controls.Add(Me.ScanPassportConnectDevice2)
        Me.Controls.Add(Me.BarcodeScannerConnectDavice1)
        Me.Controls.Add(Me.PrinterConnectDevice1)
        Me.Controls.Add(Me.CoinOutConnectDevice1)
        Me.Controls.Add(Me.CashOutConnectDevice2)
        Me.Controls.Add(Me.CashOutConnectDevice1)
        Me.Controls.Add(Me.CoinInConnectDevice1)
        Me.Controls.Add(Me.CashInConnectDevice1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Name = "Monitoring"
        Me.Text = "Monitoring"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ScanPassportConnectDevice1 As ScanPassport.ScanPassportConnectDevice
    Friend WithEvents CashInConnectDevice1 As CashIn.CashInConnectDevice
    Friend WithEvents CoinInConnectDevice1 As CoinIn.CoinInConnectDevice
    Friend WithEvents CashOutConnectDevice1 As CashOut.CashOutConnectDevice
    Friend WithEvents CashOutConnectDevice2 As CashOut.CashOutConnectDevice
    Friend WithEvents CoinOutConnectDevice1 As CoinOut.CoinOutConnectDevice
    Friend WithEvents PrinterConnectDevice1 As Printer.PrinterConnectDevice
    Friend WithEvents BarcodeScannerConnectDavice1 As BarcodeScanner.BarcodeScannerConnectDavice
    Friend WithEvents ScanPassportConnectDevice2 As ScanPassport.ScanPassportConnectDevice
    Friend WithEvents IpCamConnectDevice1 As IPCam.IPCamConnectDevice
End Class
