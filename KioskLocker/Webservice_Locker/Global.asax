﻿<%@ Application Language="VB" %>

<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup

        log4net.Config.XmlConfigurator.Configure()
        log4net.GlobalContext.Properties.Item("ApplicationCode") = "WebServiceLocker"
        log4net.GlobalContext.Properties.Item("ServerName") = Environment.MachineName
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
    End Sub

    Protected Sub Application_PostAcquireRequestState(sender As Object, e As EventArgs)
        If TypeOf Context.Handler Is IRequiresSessionState Then
            log4net.ThreadContext.Properties.Item("RemoteAddress") = HttpContext.Current.Request.UserHostAddress
        End If
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub

</script>