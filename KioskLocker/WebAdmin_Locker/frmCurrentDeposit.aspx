﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Master/MasterPage.Master" AutoEventWireup="false" CodeFile="frmCurrentDeposit.aspx.vb" Inherits="frmCurrentDeposit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContainer" Runat="Server">
    <link rel="stylesheet" href="vendor/chosen_v1.4.0/chosen.min.css">
    <link rel="stylesheet" href="vendor/checkbo/src/0.1.4/css/checkBo.min.css" />
    <style type="text/css">
        
        table.table-bordered tbody tr td.center
        {
            text-align:center;
            }
        th
        {
            background-color:#eeeeee;
            }        
        
        .table > thead > tr > th
        {
            vertical-align:top;
            }

      td {
          text-align:center;
      }

          td.left {
        text-align:left;
          }

      .table-bordered tr:hover td {
        background-color:mintcream;
      }

      .table-bordered tr td {
        background-color:transparent;
        cursor:pointer;
      }
        
    /*Check Screen To Hide/Show For Responsive Feature*/
        
    @media (max-width: 47em) {
      
     }        

     @media (min-width: 47em) {
          .no-more-tables {
                max-height:400px;
          }
     }  

  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="page-title">
        <div class="title">Current Deposit</div>
    </div>
    <asp:UpdatePanel ID="udpSearch" runat="server">
        <ContentTemplate>
            <div class="card bg-white">
                <div class="card-header">
                    Display Condition
                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                            <label class="col-sm-4 control-label-right">Location</label>
                            <div class="col-sm-8">                      
                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="chosen form-control" style="width: 100%;" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            </div>                    
                        </div>
                
                        <asp:Panel ID="pnlKiosk_On" runat="server" CssClass="col-lg-6">
                            <div class="form-group">
                            <label class="col-sm-4 control-label-right">Kiosk</label>
                            <div class="col-sm-8">                      
                                <asp:DropDownList ID="ddlKiosk" runat="server" CssClass="chosen form-control" style="width: 100%;"></asp:DropDownList>
                            </div>
                            </div>
                        </asp:Panel>   
                         <asp:Panel ID="pnlKiosk_Off" runat="server" CssClass="col-lg-6">          
                            <div class="form-group">
                                <label class="col-sm-4 control-label-right" style="color:white;">Kiosk</label>
                                <div class="col-sm-8">                      
                                   <input type="text"  style="visibility:hidden;" class="form-control"/>
                                </div>
                            </div>              
                        </asp:Panel> 
                    </div> 
                    <div class="row">
                        <div class="col-lg-6 m-t">
                            <div class="form-group">
                                <label class="col-sm-4 control-label-right">Locker Size</label>
                                <div class="col-sm-8">                      
                                    <asp:DropDownList ID="ddlCabinetModel" runat="server" CssClass="chosen form-control" style="width: 100%;"></asp:DropDownList>
                                </div>
                            </div>                    
                        </div> 

                        <div class="col-lg-6 m-t">
                            <div class="form-group">
                                <label class="col-sm-4 control-label-right">Deposit Transaction No</label>
                                <div class="col-sm-8">                      
                                    <asp:TextBox CssClass="form-control" ID="txtDepositTransactionNo" PlaceHolder="Deposit Transaction No" runat="server" />
                                </div>
                            </div>                    
                        </div>
                    </div>
                    
                    <div class="row">
                        <asp:LinkButton CssClass="btn btn-primary btn-icon loading-demo mr5 m-t btn-shadow" ID="btnApply" runat="server">
                          <i class="fa fa-check"></i>
                          <span>Apply</span>
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="udpReport" runat="server">
        <ContentTemplate>
            <div class="card bg-white">
                <div class="card-header">
                    <asp:Label ID="lblHeader" runat="server" CssClass="h4"></asp:Label>
                </div>
                <div class="card-block">
                    <div style="width: 100%; overflow: auto;">
                        <div class="no-more-tables">
                            <table class="table table-bordered m-b-0">
                                <thead>
                                    <tr>
                                        <th>DEPOSIT TRANSACTION</th>
                                        <th>PIN CODE</th>
                                        <th>Location</th>
                                        <th>KIOSK</th>
                                        <th>LOCKER</th>
                                        <th>DEPOSIT AMOUNT</th>
                                        <th>Paid Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptList" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td data-title="Deposit Transaction No" class="text-center" >
                                                    <asp:Label ID="lblDepositTransactionNo" runat="server"></asp:Label>
                                                    <asp:Label ID="lblDepositTransactionID" runat="server" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblIsFine" runat="server" Visible="false" Text="N"></asp:Label>
                                                    <asp:Label ID="lblDepositFineAmt" runat="server" Visible="false" Text="N"></asp:Label>
                                                </td>
                                                <td data-title="Pincode" class="text-center" >
                                                    <asp:Button CssClass="btn btn-success" ID="btnPincode" runat="server" Text="Pincode" Visible="false" CommandName="Pincode" />
                                                    <asp:Label ID="lblPincode" runat="server" Text="" Visible="false"></asp:Label>
                                                </td>
                                                <td data-title="Location">
                                                    <asp:Label ID="lblLocation" runat="server"></asp:Label></td>
                                                <td data-title="Kiosk">
                                                    <asp:Label ID="lblKiosk" runat="server"></asp:Label></td>
                                                <td data-title="Locker" class="text-center" >
                                                    <asp:Label ID="lblLockerName" runat="server"></asp:Label></td>
                                                <td data-title="Deposit Amount" class="text-right" >
                                                    <asp:Label ID="lblDepositAmt" runat="server"></asp:Label></td>
                                                <td data-title="Deposit Paid Time" class="text-center" >
                                                    <asp:Label ID="lblDepositPaidTime" runat="server"></asp:Label></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Panel ID="pnlDialogPincode" runat="server" CssClass="modal fade in" aria-hidden="true"
                aria-labelledby="examplePositionCenter" role="dialog" TabIndex="-1" Style="display: block;" Visible="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <asp:LinkButton ID="btnCloseDialog" runat="server" CssClass="close" style="text-decoration:none;" >
                                <span aria-hidden="true">x</span>
                            </asp:LinkButton>

                            <h4 class="modal-title">PINCODE</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <h3><asp:Label ID="lblDialogPinCode" runat="server" Text=""></asp:Label></h3>
                                    <asp:Label ID="lblDialogDepositTransactionID" runat="server" Text="0" Visible="false" ></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group ">
                                        <label class="col-sm-1"></label>
                                        <label class="col-sm-1 cb-checkbox cb-md">
                                            <asp:CheckBox ID="chkIsFine" runat="server"  />
                                        </label>
                                        <h4 class="card-title col-sm-10 control-label" style="text-align: left;">คำนวณค่าปรับเมื่อรับคืนด้วยรหัสส่วนตัว</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="col-sm-1"></label>
                                        <label class="col-sm-1"></label>
                                        <h4 class="card-title col-sm-2 control-label" style="text-align: left;">ค่าปรับ</h4>
                                        <div class="col-sm-2">
                                            <asp:TextBox CssClass="form-control" ID="txtFineAmt"  runat="server" />
                                        </div>
                                        <h4 class="card-title col-sm-1 control-label" style="text-align: left;">บาท</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="form-group" style="text-align: right">
                                <asp:LinkButton CssClass="btn btn-success btn-icon loading-demo mr5 btn-shadow" ID="btnSave" runat="server">
                                    <i class="fa fa-save"></i>
                                    <span>Save</span>
                                </asp:LinkButton>
                                <asp:LinkButton CssClass="btn btn-warning btn-icon loading-demo mr5 btn-shadow" ID="btnCancel" runat="server">
                                    <i class="fa fa-rotate-left"></i>
                                    <span>Cancel</span>
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContainer" Runat="Server">
    <!-- page scripts -->
    <script src="vendor/chosen_v1.4.0/chosen.jquery.min.js" type="text/javascript" ></script>
    <script src="vendor/jquery.tagsinput/src/jquery.tagsinput.js" type="text/javascript" ></script>
    <script src="vendor/checkbo/src/0.1.4/js/checkBo.min.js" type="text/javascript" ></script>
    <script src="vendor/intl-tel-input//build/js/intlTelInput.min.js" type="text/javascript" ></script>
    <script src="vendor/moment/min/moment.min.js" type="text/javascript" ></script>
    <script src="vendor/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript" ></script>
    <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript" ></script>
    <script src="vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript" ></script>
    <script src="vendor/clockpicker/dist/jquery-clockpicker.min.js" type="text/javascript" ></script>
    <script src="vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js" type="text/javascript" ></script>
    <script src="vendor/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript" ></script>
    <script src="vendor/select2/dist/js/select2.js" type="text/javascript" ></script>
    <script src="vendor/selectize/dist/js/standalone/selectize.min.js" type="text/javascript" ></script>
    <script src="vendor/jquery-labelauty/source/jquery-labelauty.js" type="text/javascript" ></script>
    <script src="vendor/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript" ></script>
    <script src="vendor/typeahead.js/dist/typeahead.bundle.js" type="text/javascript" ></script>
    <script src="vendor/multiselect/js/jquery.multi-select.js" type="text/javascript" ></script>
    <!-- end page scripts -->

    <!-- initialize page scripts -->
    <script src="scripts/forms/plugins.js" type="text/javascript" ></script>
    <!--end initialize page scripts -->
</asp:Content>

