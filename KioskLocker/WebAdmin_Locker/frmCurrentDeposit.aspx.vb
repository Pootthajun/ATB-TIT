﻿Imports System.Data
Imports System.Data.SqlClient
Imports ServerLinqDB.ConnectDB
Imports ServerLinqDB.TABLE
Partial Class frmCurrentDeposit
    Inherits System.Web.UI.Page

    Dim BL As New LockerBL

    Const FunctionalID As Int16 = 30
    Const FunctionalZoneID As Int16 = 6

    Private Sub frmCurrentDeposit_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim ufDt As DataTable = DirectCast(Session("List_User_Functional"), DataTable)
        If ufDt Is Nothing Then
            Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
        End If
        If ufDt.Rows.Count > 0 Then
            Dim auDt As DataTable = BL.GetList_User_Functional(1, FunctionalZoneID, FunctionalID, Session("Username"))
            If auDt.Rows.Count = 0 Then
                Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
                Exit Sub
            End If
            auDt.Dispose()
        End If
        ufDt.Dispose()


        If Not IsPostBack Then
            If Session("UserName") IsNot Nothing Then
                Dim UserName As String = Session("UserName")

                BindSearchForm(UserName)
                BindList()
                BL.SetTextIntKeypress(txtFineAmt)
            End If
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Init", "initFormPlugin();", True)
        End If
    End Sub

    Private Sub BindSearchForm(UserName As String)
        BL.Bind_DDL_Location(ddlLocation, UserName)
        ddlLocation.Items(0).Text = "All Locations"
        ddlLocation_SelectedIndexChanged(ddlLocation, Nothing)

        BL.Bind_DDL_CabinetModel(ddlCabinetModel)
        ddlCabinetModel.Items(0).Text = "All Sizes"
    End Sub

    Private Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If Session("UserName") IsNot Nothing Then
            pnlKiosk_On.Visible = ddlLocation.SelectedIndex > 0
            pnlKiosk_Off.Visible = Not pnlKiosk_On.Visible
            If pnlKiosk_On.Visible Then Bind_Kiosk(Session("UserName"))
        End If
    End Sub

    Private Sub Bind_Kiosk(UserName As String)
        BL.Bind_DDL_Kiosk(UserName, ddlKiosk, ddlLocation.SelectedValue)

        ddlKiosk.Items(0).Text = "All machine(s)"
    End Sub

    Private Sub btnApply_Click(sender As Object, e As EventArgs) Handles btnApply.Click
        BindList()
    End Sub

    Private Sub BindList()
        lblHeader.Text = "รายการฝากและยังไม่รับคืน "

        Dim parm(4) As SqlParameter
        Dim wh As String = " and deposit_status <> '0' "

        If ddlKiosk.SelectedValue <> "" Then
            wh += " and ms_kiosk_id=@_KIOSK_ID"
            parm(1) = ServerDB.SetBigInt("@_KIOSK_ID", ddlKiosk.SelectedValue)
            lblHeader.Text &= "สำหรับตู้ " & ddlKiosk.Items(ddlKiosk.SelectedIndex).Text & " "
        End If

        If ddlLocation.SelectedValue.Trim <> "" Then
            wh += " and ms_location_id=@_LOCATION_ID " + Environment.NewLine
            parm(0) = ServerDB.SetBigInt("@_LOCATION_ID", ddlLocation.SelectedValue)

            lblHeader.Text &= "ที่ " & ddlLocation.Items(ddlLocation.SelectedIndex).Text & " "
        Else
            If Session("List_User_LocationID") IsNot Nothing Then
                wh += " and ms_location_id in (" & Session("List_User_LocationID") & ")"
                lblHeader.Text &= "ทุก Location "
            Else
                Response.Redirect(System.Web.Security.FormsAuthentication.DefaultUrl)
            End If
        End If

        If txtDepositTransactionNo.Text.Trim <> "" Then
            wh += " and deposit_transaction_no like '%' + @_DEPOSIT_TRANS_NO + '%'"
            parm(2) = ServerDB.SetText("@_DEPOSIT_TRANS_NO", txtDepositTransactionNo.Text.Trim)
            lblHeader.Text &= " Deposit Transaction " & txtDepositTransactionNo.Text.Trim & " "
        End If

        If ddlCabinetModel.SelectedValue <> "" Then
            wh += " and ms_cabinet_model_id=@_CABINET_MODEL_ID"
            parm(3) = ServerDB.SetBigInt("@_CABINET_MODEL_ID", ddlCabinetModel.SelectedValue)
            lblHeader.Text &= " ขนาด " & ddlCabinetModel.Items(ddlCabinetModel.SelectedIndex).Text & " "
        End If


        Dim sql As String = " select * from v_transaction_log"
        sql += " where deposit_status= '1' and collect_transaction_no is null "  'ต้องเป็นรายการฝากที่ยังไม่ได้รับคืน
        sql += " " & wh
        sql += " and convert(varchar(8),getdate(),112) between convert(varchar(8),valid_start_date,112) and convert(varchar(8),valid_expire_date,112) " & vbNewLine

        Dim dt As DataTable = ServerDB.ExecuteTable(sql, parm)
        'Session("CurrentDeposit") = dt

        rptList.DataSource = dt
        rptList.DataBind()

        If dt.Rows.Count = 0 Then
            lblHeader.Text &= "ไม่พบข้อมูล "
        Else
            lblHeader.Text &= " พบ " & dt.Rows.Count & " Record(s)"
        End If
    End Sub

    Private Sub rptList_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptList.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub
        Dim lblDepositTransactionNo As Label = e.Item.FindControl("lblDepositTransactionNo")
        Dim lblDepositTransactionID As Label = e.Item.FindControl("lblDepositTransactionID")
        Dim btnPincode As Button = e.Item.FindControl("btnPincode")
        Dim lblPincode As Label = e.Item.FindControl("lblPincode")
        Dim lblLocation As Label = e.Item.FindControl("lblLocation")
        Dim lblKiosk As Label = e.Item.FindControl("lblKiosk")
        Dim lblLockerName As Label = e.Item.FindControl("lblLockerName")
        Dim lblDepositAmt As Label = e.Item.FindControl("lblDepositAmt")
        Dim lblDepositPaidTime As Label = e.Item.FindControl("lblDepositPaidTime")
        Dim lblIsFine As Label = e.Item.FindControl("lblIsFine")
        Dim lblDepositFineAmt As Label = e.Item.FindControl("lblDepositFineAmt")

        Dim en_US As New Globalization.CultureInfo("en-US")
        lblDepositTransactionNo.Text = e.Item.DataItem("deposit_transaction_no")
        lblDepositTransactionID.Text = e.Item.DataItem("tb_service_transaction_id")
        If Convert.IsDBNull(e.Item.DataItem("pin_code")) = False Then
            lblPincode.Text = e.Item.DataItem("pin_code")
            btnPincode.Visible = True
        End If
        lblLocation.Text = e.Item.DataItem("location_name")
        lblKiosk.Text = e.Item.DataItem("com_name")
        If Convert.IsDBNull(e.Item.DataItem("locker_name")) = False Then lblLockerName.Text = e.Item.DataItem("locker_name")
        lblDepositAmt.Text = e.Item.DataItem("deposit_amt")
        If Convert.IsDBNull(e.Item.DataItem("deposit_paid_time")) = False Then lblDepositPaidTime.Text = Convert.ToDateTime(e.Item.DataItem("deposit_paid_time")).ToString("MMM dd yyyy HH:mm:ss", en_US)
        If Convert.IsDBNull(e.Item.DataItem("is_fine")) = False Then lblIsFine.Text = e.Item.DataItem("is_fine")
        If Convert.IsDBNull(e.Item.DataItem("deposit_fine_amt")) = False Then lblDepositFineAmt.Text = e.Item.DataItem("deposit_fine_amt")
    End Sub

    Private Function GetDefaultFineRate(DepositTransactionID As Long) As Integer
        Dim ret As Long = 0
        Dim sql As String = "select t.ms_kiosk_id, c.ms_cabinet_model_id "
        sql += " from TB_SERVICE_TRANSACTION t"
        sql += " inner join MS_LOCKER l on l.id=t.ms_locker_id "
        sql += " inner join MS_CABINET c on c.id=l.ms_cabinet_id"
        sql += " where t.id=@_DEPOSIT_TRANSACTION_ID"
        Dim p(1) As SqlParameter
        p(0) = ServerDB.SetBigInt("@_DEPOSIT_TRANSACTION_ID", DepositTransactionID)

        Dim dt As DataTable = ServerDB.ExecuteTable(sql, p)
        If dt.Rows.Count > 0 Then
            Dim KioskId As Long = dt.Rows(0)("ms_kiosk_id")
            Dim CabinetModelId As Long = dt.Rows(0)("ms_cabinet_model_id")

            sql = "select top 1 rf.fine_rate " & Environment.NewLine
            sql += " from MS_SERVICE_RATE_FINE rf " & Environment.NewLine
            sql += " inner join MS_SERVICE_RATE r on r.id=rf.ms_service_rate_id " & Environment.NewLine
            sql += " inner join MS_KIOSK k on k.ms_location_id=r.ms_location_id " & Environment.NewLine
            sql += " where k.id=@_KIOSK_ID and rf.ms_cabinet_model_id = @_CABINET_MODEL_ID "

            ReDim p(2)
            p(0) = ServerDB.SetBigInt("@_KIOSK_ID", KioskId)
            p(1) = ServerDB.SetBigInt("@_CABINET_MODEL_ID", CabinetModelId)

            dt = ServerDB.ExecuteTable(sql, p)
            If dt.Rows.Count > 0 Then
                ret = dt.Rows(0)("fine_rate")
            End If
        End If
        dt.Dispose()

        Return ret
    End Function

    Private Sub rptList_ItemCommand(source As Object, e As RepeaterCommandEventArgs) Handles rptList.ItemCommand

        Dim lblDepositTransactionID As Label = e.Item.FindControl("lblDepositTransactionID")
        Dim lblPincode As Label = e.Item.FindControl("lblPincode")
        Dim lblIsFine As Label = e.Item.FindControl("lblIsFine")
        Dim lblDepositFineAmt As Label = e.Item.FindControl("lblDepositFineAmt")
        Select Case e.CommandName
            Case "Pincode"
                lblDialogPinCode.Text = lblPincode.Text
                lblDialogDepositTransactionID.Text = lblDepositTransactionID.Text
                chkIsFine.Checked = (lblIsFine.Text = "Y")

                If lblDepositFineAmt.Text = 0 Then
                    txtFineAmt.Text = GetDefaultFineRate(lblDepositTransactionID.Text)
                Else
                    txtFineAmt.Text = lblDepositFineAmt.Text
                End If

                pnlDialogPincode.Visible = True
        End Select
    End Sub

    Private Sub ClearPincodeDialog()
        lblDialogPinCode.Text = ""
        lblDialogDepositTransactionID.Text = "0"
        chkIsFine.Checked = False
        pnlDialogPincode.Visible = False
        txtFineAmt.Text = 0
    End Sub

    Private Sub btnCloseDialog_Click(sender As Object, e As EventArgs) Handles btnCloseDialog.Click
        ClearPincodeDialog()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        ClearPincodeDialog()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If chkIsFine.Checked = True Then
            If txtFineAmt.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('กรุณาระบุจำนวนเงินค่าปรับ');", True)
                Exit Sub
            End If
        End If


        Dim trans As New ServerTransactionDB
        Dim lnq As New TbServiceTransactionServerLinqDB
        lnq.GetDataByPK(lblDialogDepositTransactionID.Text, trans.Trans)
        If lnq.ID > 0 Then
            lnq.IS_FINE = IIf(chkIsFine.Checked = True, "Y", "N")
            lnq.FINE_AMT = txtFineAmt.Text

            Dim ret As ExecuteDataInfo = lnq.UpdateData(Session("UserName"), trans.Trans)
            If ret.IsSuccess = True Then
                trans.CommitTransaction()
                ClearPincodeDialog()
                BindList()
            Else
                trans.RollbackTransaction()
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('" & ret.ErrorMessage & "');", True)
                Exit Sub
            End If
        Else
            trans.RollbackTransaction()
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", "alert('No data found');", True)
            Exit Sub
        End If
        lnq = Nothing
    End Sub
End Class
