﻿Imports System.Data.SqlClient
Imports Kiosk_Locker.Data
Imports Kiosk_Locker.Data.KioskConfigData
Imports KioskLinqDB.ConnectDB
Imports KioskLinqDB.TABLE
Public Class frmCollectSelectDocument

    Dim TimeOut As Int32 = KioskConfig.TimeOutSec
    Dim TimeOutCheckTime As DateTime = DateTime.Now

    Private Sub frmPickupSelectDocument_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ControlBox = False
        Me.BackColor = bgColor
        Me.WindowState = FormWindowState.Maximized

        KioskConfig.SelectForm = Data.KioskConfigData.KioskLockerForm.CollectSelectDocument
        SetChildFormLanguage()
        SetLabelNotificationText()
    End Sub

    Private Sub frmPickupSelectDocument_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        frmMain.pnlAds.Visible = False
        frmMain.pnlFooter.Visible = True
        frmMain.pnlCancel.Visible = True
        Application.DoEvents()

        frmDepositSelectLocker.MdiParent = frmMain
        frmDepositSelectLocker.LoadLockerList(True)

        InsertLogTransactionActivity("", Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupSelectDoc_OpenForm, "", False)
    End Sub

    Private Sub lblQRCode_Click(sender As Object, e As EventArgs) Handles lblQRCode.Click, pnlQRCode.Click
        InsertLogTransactionActivity("", Collect.TransactionNo, "", KioskLockerForm.Home, KioskLockerStep.PickupSelectDoc_ClickQRCode, "", False)

        Dim f As New frmCollectScanQRCode
        f.MdiParent = frmMain
        f.Show()
        frmMain.btnPointer.Visible = False
        frmMain.TimerCheckOpenClose.Enabled = False
        Me.Close()
        Application.DoEvents()
        SendKioskAlarm("KIOSK_OUT_OF_SERVICE", False)
    End Sub

    Private Sub lblPassword_Click(sender As Object, e As EventArgs) Handles lblPassword.Click, pnlPassword.Click
        InsertLogTransactionActivity("", Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupSelectDoc_ClickPassword, "", False)
        Collect.LostQRCode = "Y"
        Dim ret As ExecuteDataInfo = UpdateCollectTransaction(Collect)
        If ret.IsSuccess = True Then
            frmLoading.Show(frmMain)
            Application.DoEvents()

            frmDepositSelectLocker.Show()
            'Application.DoEvents()

            Me.Close()
            frmLoading.Close()
        Else
            InsertLogTransactionActivity("", Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupSelectDoc_ClickIDCard, " Update Pickup Data Fail", True)
        End If
    End Sub

    Private Sub TimerTimeOut_Tick(sender As Object, e As EventArgs) Handles TimerTimeOut.Tick
        'TimeOut = TimeOut - 1
        Application.DoEvents()
        'lblTimeOut.Text = TimeOut
        If TimeOutCheckTime.AddSeconds(TimeOut) <= DateTime.Now Then
            InsertLogTransactionActivity("", Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupSelectDoc_Timeout, "", False)
            TimerTimeOut.Enabled = False
            UpdateCollectStatus(Collect.CollectTransactionID, CollectTransactionData.TransactionStatus.TimeOut, KioskLockerStep.PickupSelectDoc_Timeout)

            frmMain.CloseAllChildForm()
            Dim f As New frmHome
            f.MdiParent = frmMain
            f.Show()
        End If
    End Sub

    Public Sub SetLabelNotificationText()
        'แสดง Status ของเครื่องอ่าน QR Code และ เครื่องอ่านบัตรประชาชน
        Dim dvDt As DataTable = GetStatusAllDeviceDT()
        If dvDt.Rows.Count > 0 Then
            dvDt.DefaultView.RowFilter = "device_id=" & DeviceID.QRCodeReader & " and ms_device_status_id<>1"
            If dvDt.DefaultView.Count > 0 Then
                lblQRCodeNotification.Text = GetNotificationText(5)
                pnlQRCode.BackgroundImage = My.Resources.IconPickupQRCodeFail
                RemoveHandler pnlQRCode.Click, AddressOf lblQRCode_Click
                RemoveHandler lblQRCode.Click, AddressOf lblQRCode_Click
                lblQRCode.Enabled = False
                lblQRCodeNotification.Visible = True
            End If
            dvDt.DefaultView.RowFilter = ""

            'dvDt.DefaultView.RowFilter = "device_id=" & DeviceID.IDCardPassportScanner & " and ms_device_status_id<>1"
            'If dvDt.DefaultView.Count > 0 Then
            '    lblLabelIDCardNotification.Text = GetNotificationText(6)
            '    pnlPassword.BackgroundImage = My.Resources.IconPickupIDCardFail
            '    RemoveHandler pnlPassword.Click, AddressOf lblIDCard_Click
            '    RemoveHandler lblIDCard.Click, AddressOf lblIDCard_Click
            '    RemoveHandler lblPassword.Click, AddressOf lblIDCard_Click
            '    lblIDCard.Enabled = False
            '    lblPassword.Enabled = False
            '    lblLabelIDCardNotification.Visible = True
            'End If
            'dvDt.DefaultView.RowFilter = ""

            Application.DoEvents()
        End If
        dvDt.Dispose()
    End Sub
End Class