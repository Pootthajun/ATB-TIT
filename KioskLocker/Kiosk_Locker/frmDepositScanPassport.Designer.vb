﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmDepositScanPassport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.TimerScan = New System.Windows.Forms.Timer(Me.components)
        Me.lblScanFail = New System.Windows.Forms.Label()
        Me.TimerTimeOut = New System.Windows.Forms.Timer(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.pbIconScan = New System.Windows.Forms.PictureBox()
        Me.lblPleaseWait = New System.Windows.Forms.Label()
        Me.pnlProcessing = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbIconScan, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlProcessing.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.Font = New System.Drawing.Font("Thai Sans Lite", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(12, 50)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(1000, 73)
        Me.lblTitle.TabIndex = 13
        Me.lblTitle.Text = "สแกน บัตรประชาชน / หนังสือเดินทาง"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblTitle.Visible = False
        '
        'TimerScan
        '
        Me.TimerScan.Interval = 4000
        '
        'lblScanFail
        '
        Me.lblScanFail.BackColor = System.Drawing.Color.Transparent
        Me.lblScanFail.Font = New System.Drawing.Font("Thai Sans Lite", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblScanFail.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblScanFail.Location = New System.Drawing.Point(73, 122)
        Me.lblScanFail.Name = "lblScanFail"
        Me.lblScanFail.Size = New System.Drawing.Size(879, 39)
        Me.lblScanFail.TabIndex = 14
        Me.lblScanFail.Text = "การอ่านค่าไม่สำเร็จ กรุณาลองอีกครั้ง"
        Me.lblScanFail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblScanFail.Visible = False
        '
        'TimerTimeOut
        '
        Me.TimerTimeOut.Interval = 1000
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Kiosk_Locker.My.Resources.Resources.IconProcessing
        Me.PictureBox1.Location = New System.Drawing.Point(202, 67)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(500, 400)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 16
        Me.PictureBox1.TabStop = False
        '
        'pbIconScan
        '
        Me.pbIconScan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.pbIconScan.Image = Global.Kiosk_Locker.My.Resources.Resources.IconScanPassport
        Me.pbIconScan.Location = New System.Drawing.Point(73, 156)
        Me.pbIconScan.Name = "pbIconScan"
        Me.pbIconScan.Size = New System.Drawing.Size(879, 389)
        Me.pbIconScan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbIconScan.TabIndex = 15
        Me.pbIconScan.TabStop = False
        '
        'lblPleaseWait
        '
        Me.lblPleaseWait.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPleaseWait.Font = New System.Drawing.Font("Thai Sans Lite", 30.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPleaseWait.Location = New System.Drawing.Point(3, 19)
        Me.lblPleaseWait.Name = "lblPleaseWait"
        Me.lblPleaseWait.Size = New System.Drawing.Size(899, 57)
        Me.lblPleaseWait.TabIndex = 17
        Me.lblPleaseWait.Text = "กรุณารอสักครู่"
        Me.lblPleaseWait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlProcessing
        '
        Me.pnlProcessing.Controls.Add(Me.lblPleaseWait)
        Me.pnlProcessing.Controls.Add(Me.PictureBox1)
        Me.pnlProcessing.Location = New System.Drawing.Point(446, 247)
        Me.pnlProcessing.Name = "pnlProcessing"
        Me.pnlProcessing.Size = New System.Drawing.Size(905, 507)
        Me.pnlProcessing.TabIndex = 18
        Me.pnlProcessing.Visible = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(12, 212)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 19
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'frmDepositScanPassport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1024, 553)
        Me.Controls.Add(Me.lblScanFail)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.pnlProcessing)
        Me.Controls.Add(Me.pbIconScan)
        Me.Controls.Add(Me.lblTitle)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmDepositScanPassport"
        Me.Text = "frmChangeLanguage"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbIconScan, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlProcessing.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblTitle As Label
    Friend WithEvents TimerScan As Timer
    Friend WithEvents lblScanFail As Label
    Friend WithEvents pbIconScan As PictureBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents TimerTimeOut As Timer
    Friend WithEvents lblPleaseWait As Label
    Friend WithEvents pnlProcessing As Panel
    Friend WithEvents Button1 As Button
End Class
