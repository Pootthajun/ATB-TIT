﻿Imports Kiosk_Locker.Data.KioskConfigData
Public Class frmDepositSelectLocker
    Dim TimeOut As Int32 = KioskConfig.TimeOutSec
    Dim TimeOutCheckTime As DateTime = DateTime.Now

    Private Sub frmDepositSelectLocker_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.ControlBox = False
        Me.BackColor = bgColor
        If ServiceID = Data.ConstantsData.TransactionType.DepositBelonging Then
            KioskConfig.SelectForm = KioskLockerForm.DepositSelectLocker
        Else
            KioskConfig.SelectForm = KioskLockerForm.CollectSelectLocker
        End If

        SetChildFormLanguage()
    End Sub
    Private Sub frmDepositSelectLocker_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Me.WindowState = FormWindowState.Maximized
        frmMain.pnlAds.Visible = False
        frmMain.pnlFooter.Visible = True
        frmMain.pnlCancel.Visible = True

        Application.DoEvents()
        InsertLogTransactionActivity(Customer.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.DepositSelectLocker_OpenForm, "", False)
        TimeOutCheckTime = DateTime.Now
        TimerTimeOut.Enabled = True

    End Sub

    Dim AllPadding As Integer = 1
    Dim PcWidth As Integer = 75

    Public Sub LoadLockerList(IsCollect As Boolean)
        Try
            InsertLogTransactionActivity(Customer.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.DepositSelectLocker_LoadLockerList, "", False)
            Dim cbWith As Integer = 0

            UcCabinet1.LoadCabinetData(False, CabinetList, IsCollect)
            If UcCabinet1.LockerActiveQty > 0 Then
                cbWith += UcCabinet1.Width + AllPadding
            End If

            UcCabinet2.LoadCabinetData(False, CabinetList, IsCollect)
            If UcCabinet2.LockerActiveQty > 0 Then
                cbWith += UcCabinet2.Width + AllPadding
            End If

            UcCabinet3.LoadCabinetData(False, CabinetList, IsCollect)
            If UcCabinet3.LockerActiveQty > 0 Then
                cbWith += UcCabinet3.Width + AllPadding
            End If

            UcCabinet4.LoadCabinetData(False, CabinetList, IsCollect)
            If UcCabinet4.LockerActiveQty > 0 Then
                cbWith += UcCabinet4.Width + AllPadding
            End If

            UcCabinet5.LoadCabinetData(False, CabinetList, IsCollect)
            If UcCabinet5.LockerActiveQty > 0 Then
                cbWith += UcCabinet5.Width + AllPadding
            End If

            UcCabinet6.LoadCabinetData(False, CabinetList, IsCollect)
            If UcCabinet6.LockerActiveQty > 0 Then
                cbWith += UcCabinet6.Width + AllPadding
            End If

            UcCabinet7.LoadCabinetData(False, CabinetList, IsCollect)
            If UcCabinet7.LockerActiveQty > 0 Then
                cbWith += UcCabinet7.Width + AllPadding
            End If

            UcCabinet8.LoadCabinetData(False, CabinetList, IsCollect)
            If UcCabinet8.LockerActiveQty > 0 Then
                cbWith += UcCabinet8.Width + AllPadding
            End If

            UcCabinet9.LoadCabinetData(False, CabinetList, IsCollect)
            If UcCabinet9.LockerActiveQty > 0 Then
                cbWith += UcCabinet9.Width + AllPadding
            Else
                UcCabinet9.Visible = False
            End If

            UcCabinet10.LoadCabinetData(False, CabinetList, IsCollect)
            If UcCabinet10.LockerActiveQty > 0 Then
                cbWith += UcCabinet10.Width + AllPadding
            Else
                UcCabinet10.Visible = False
            End If


            'วิธีการคำนวณ 
            'ความกว้างทั้งหมดคือ (oQty*61)
            Dim pLeft As Integer = (pnlCabinetLayout.Width / 2) - ((cbWith + PcWidth + AllPadding) / 2)
            pLeft = SetCabinetPosition(UcCabinet1, pLeft, 1)
            pLeft = SetCabinetPosition(UcCabinet2, pLeft, 2)
            pLeft = SetCabinetPosition(UcCabinet3, pLeft, 3)
            pLeft = SetCabinetPosition(UcCabinet4, pLeft, 4)
            pLeft = SetCabinetPosition(UcCabinet5, pLeft, 5)
            pLeft = SetCabinetPosition(UcCabinet6, pLeft, 6)
            pLeft = SetCabinetPosition(UcCabinet7, pLeft, 7)
            pLeft = SetCabinetPosition(UcCabinet8, pLeft, 8)
            pLeft = SetCabinetPosition(UcCabinet9, pLeft, 9)
            pLeft = SetCabinetPosition(UcCabinet10, pLeft, 10)

            If IsCollect = True Then
                'สลับตำแหน่ง Legent
                lblNotAvailable.Location = New Point(934, 484)
                lblAvailable.Location = New Point(934, 510)
            End If
            Application.DoEvents()
        Catch ex As Exception
            InsertErrorLog("Exception : " & ex.Message & vbNewLine & ex.StackTrace, Customer.DepositTransNo, Collect.TransactionNo, 0, KioskConfig.SelectForm, KioskLockerStep.DepositSelectLocker_LoadLockerList)
        End Try

    End Sub

    Private Function SetCabinetPosition(uc As ucCabinet, pLeft As Integer, ucSeq As Integer) As Integer
        If KioskConfig.LockerPCPosition = ucSeq Then
            pnlLayoutPC.Left = pLeft
            pLeft = pLeft + PcWidth + AllPadding
        End If

        If uc.LockerActiveQty > 0 Then
            uc.Left = pLeft
            pLeft = pLeft + uc.Width + AllPadding
        Else
            uc.Visible = False
        End If
        Application.DoEvents()
        Return pLeft
    End Function



    Private Sub UcCabinet1_LockerClick(f As ucLockerInfo) Handles UcCabinet1.LockerClick, UcCabinet2.LockerClick, UcCabinet3.LockerClick, UcCabinet4.LockerClick, UcCabinet5.LockerClick, UcCabinet6.LockerClick, UcCabinet7.LockerClick, UcCabinet8.LockerClick, UcCabinet9.LockerClick, UcCabinet10.LockerClick
        If ServiceID = Data.ConstantsData.TransactionType.DepositBelonging Then
            If f.LockerAvailable = ucLockerInfo.AvailableStatus.Availabled Then
                TimeOutCheckTime = DateTime.Now
                TimerTimeOut.Enabled = False
                TimerTimeOut.Stop()

                frmLoading.Show(frmMain)
                InsertLogTransactionActivity(Customer.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.DepositSelectLocker_SelectLocker, " " & f.txtLockerName.Text, False)

                Application.DoEvents()

                Customer.LockerID = f.LockerID
                Customer.LockerName = f.txtLockerName.Text
                Customer.CabinetID = f.CabinetID
                Customer.CabinetModelID = f.CabinetModelID
                Customer.LockerPinSolenoid = f.cbSolenoidPin.SelectedValue
                Customer.LockerPinLED = f.cbLEDPin.SelectedValue
                Customer.LockerPinSendor = f.cbSensorPin.SelectedValue

                'เลือกช่องฝากแล้วก็ Update Transaction โลด
                If UpdateServiceTransaction(Customer).IsSuccess = True Then
                    Me.Close()

                    frmDepositSetPINCode.MdiParent = frmMain
                    frmDepositSetPINCode.Show()
                    frmLoading.Close()
                End If
            ElseIf f.LockerAvailable = ucLockerInfo.AvailableStatus.NotAvailable AndAlso f.LockerAvailable = ucLockerInfo.AvailableStatus.InActive Then
                'ถ้าสถานะ ไม่ว่าง ก็ให้อยู่นิ่งๆ ไม่ต้องทำอะไร
            End If
        ElseIf ServiceID = Data.ConstantsData.TransactionType.CollectBelonging Then
            'กรณีรับคืนจาก Pincode ให้แสดงหน้าจอสำหรับกรอก Pincode

            If f.LockerAvailable = ucLockerInfo.AvailableStatus.NotAvailable Then
                frmLoading.Show(frmMain)
                Application.DoEvents()

                TimeOutCheckTime = DateTime.Now
                TimerTimeOut.Enabled = False
                TimerTimeOut.Stop()

                InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, StaffConsole.TransNo, KioskConfig.SelectForm, KioskLockerStep.CollectSelectLocker_SelectLocker, " " & f.txtLockerName.Text, False)

                Collect.PickupTime = DateTime.Now
                Collect.LostQRCode = "Y"
                Collect.LockerID = f.LockerID

                If UpdateCollectTransaction(Collect).IsSuccess = True Then
                    Me.Close()
                    Application.DoEvents()
                    WebCam = New WebCamera.DSCamCapture
                    frmDepositSetPINCode.MdiParent = frmMain
                    frmDepositSetPINCode.Show()
                Else
                    frmLoading.Close()
                End If
            ElseIf f.LockerAvailable = ucLockerInfo.AvailableStatus.Availabled AndAlso f.LockerAvailable = ucLockerInfo.AvailableStatus.InActive Then
                'กรณีรับคืนจาก StaffConsole ถ้าเป็นตู้ที่ว่างอยู่ ก็ไม่ให้คลิกได้ เพราะจะรับคืน จะไปคลิกตู้ว่างทำไม
            End If
        End If

    End Sub

    Private Sub TimerTimeOut_Tick(sender As Object, e As EventArgs) Handles TimerTimeOut.Tick
        If KioskConfig.SelectForm = KioskLockerForm.DepositSelectLocker Then
            Application.DoEvents()
            'lblTimeOut.Text = TimeOut
            If TimeOutCheckTime.AddSeconds(TimeOut) <= DateTime.Now Then
                TimerTimeOut.Enabled = False
                TimerTimeOut.Stop()

                UpdateDepositStatus(Customer.ServiceTransactionID, DepositTransactionData.TransactionStatus.TimeOut, KioskLockerStep.DepositSelectLocker_SelectLocker)
                InsertLogTransactionActivity(Customer.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.DepositSelectLocker_SelectLocker, " ลูกค้าไม่ทำรายการภายในเวลาที่กำหนด", False)

                frmMain.CloseAllChildForm()
                Dim f As New frmHome
                f.MdiParent = frmMain
                f.Show()
            End If
        ElseIf KioskConfig.SelectForm = KioskLockerForm.CollectSelectLocker Then
            Application.DoEvents()
            'lblTimeOut.Text = TimeOut
            If TimeOutCheckTime.AddSeconds(TimeOut) <= DateTime.Now Then
                TimerTimeOut.Enabled = False
                TimerTimeOut.Stop()

                UpdateCollectStatus(Collect.CollectTransactionID, CollectTransactionData.TransactionStatus.TimeOut, KioskLockerStep.CollectSelectLocker_SelectLocker)
                InsertLogTransactionActivity(Customer.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.CollectSelectLocker_SelectLocker, " ลูกค้าไม่ทำรายการภายในเวลาที่กำหนด", False)

                frmMain.CloseAllChildForm()
                Dim f As New frmHome
                f.MdiParent = frmMain
                f.Show()
            End If
        End If

    End Sub
End Class