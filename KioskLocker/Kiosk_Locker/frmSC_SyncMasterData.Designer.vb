﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmSC_SyncMasterData
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lblHeader = New System.Windows.Forms.Label()
        Me.flpServiceRateHour = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnClose = New System.Windows.Forms.Panel()
        Me.lblClose = New System.Windows.Forms.Label()
        Me.pnSave = New System.Windows.Forms.Panel()
        Me.lblSave = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblDepositLR = New System.Windows.Forms.Label()
        Me.lblDepositMR = New System.Windows.Forms.Label()
        Me.lblDepositSR = New System.Windows.Forms.Label()
        Me.lblNextDayLR = New System.Windows.Forms.Label()
        Me.lblNextDayMR = New System.Windows.Forms.Label()
        Me.lblNextDaySR = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnClose.SuspendLayout()
        Me.pnSave.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblHeader
        '
        Me.lblHeader.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblHeader.BackColor = System.Drawing.Color.Transparent
        Me.lblHeader.Font = New System.Drawing.Font("Microsoft Sans Serif", 60.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblHeader.ForeColor = System.Drawing.Color.White
        Me.lblHeader.Location = New System.Drawing.Point(41, 48)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Size = New System.Drawing.Size(939, 97)
        Me.lblHeader.TabIndex = 44
        Me.lblHeader.Text = "Service Rate"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'flpServiceRateHour
        '
        Me.flpServiceRateHour.AutoScroll = True
        Me.flpServiceRateHour.Location = New System.Drawing.Point(57, 189)
        Me.flpServiceRateHour.Margin = New System.Windows.Forms.Padding(0)
        Me.flpServiceRateHour.Name = "flpServiceRateHour"
        Me.flpServiceRateHour.Size = New System.Drawing.Size(450, 487)
        Me.flpServiceRateHour.TabIndex = 45
        '
        'btnClose
        '
        Me.btnClose.BackgroundImage = Global.Kiosk_Locker.My.Resources.Resources.btnColWhite
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnClose.Controls.Add(Me.lblClose)
        Me.btnClose.Location = New System.Drawing.Point(810, 699)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(150, 41)
        Me.btnClose.TabIndex = 50
        '
        'lblClose
        '
        Me.lblClose.BackColor = System.Drawing.Color.Transparent
        Me.lblClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblClose.ForeColor = System.Drawing.Color.Black
        Me.lblClose.Location = New System.Drawing.Point(16, 6)
        Me.lblClose.Name = "lblClose"
        Me.lblClose.Size = New System.Drawing.Size(122, 27)
        Me.lblClose.TabIndex = 46
        Me.lblClose.Text = "Close"
        Me.lblClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnSave
        '
        Me.pnSave.BackgroundImage = Global.Kiosk_Locker.My.Resources.Resources.btnColWhite
        Me.pnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pnSave.Controls.Add(Me.lblSave)
        Me.pnSave.Location = New System.Drawing.Point(649, 699)
        Me.pnSave.Name = "pnSave"
        Me.pnSave.Size = New System.Drawing.Size(150, 41)
        Me.pnSave.TabIndex = 49
        '
        'lblSave
        '
        Me.lblSave.BackColor = System.Drawing.Color.Transparent
        Me.lblSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblSave.ForeColor = System.Drawing.Color.Black
        Me.lblSave.Location = New System.Drawing.Point(16, 6)
        Me.lblSave.Name = "lblSave"
        Me.lblSave.Size = New System.Drawing.Size(122, 27)
        Me.lblSave.TabIndex = 46
        Me.lblSave.Text = "Sync Data"
        Me.lblSave.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(57, 160)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 30)
        Me.Label1.TabIndex = 51
        Me.Label1.Text = "Hours"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(156, 160)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 30)
        Me.Label2.TabIndex = 52
        Me.Label2.Text = "SR"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(256, 160)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 30)
        Me.Label3.TabIndex = 53
        Me.Label3.Text = "MR"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.White
        Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(356, 160)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 30)
        Me.Label4.TabIndex = 54
        Me.Label4.Text = "LR"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.White
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(809, 189)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 30)
        Me.Label5.TabIndex = 57
        Me.Label5.Text = "LR"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.White
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.Location = New System.Drawing.Point(710, 189)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 30)
        Me.Label6.TabIndex = 56
        Me.Label6.Text = "MR"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.White
        Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.Location = New System.Drawing.Point(611, 189)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(100, 30)
        Me.Label7.TabIndex = 55
        Me.Label7.Text = "SR"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.White
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.Location = New System.Drawing.Point(611, 160)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(298, 30)
        Me.Label8.TabIndex = 58
        Me.Label8.Text = "Deposit"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDepositLR
        '
        Me.lblDepositLR.BackColor = System.Drawing.Color.White
        Me.lblDepositLR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDepositLR.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblDepositLR.Location = New System.Drawing.Point(809, 218)
        Me.lblDepositLR.Name = "lblDepositLR"
        Me.lblDepositLR.Size = New System.Drawing.Size(100, 30)
        Me.lblDepositLR.TabIndex = 61
        Me.lblDepositLR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDepositMR
        '
        Me.lblDepositMR.BackColor = System.Drawing.Color.White
        Me.lblDepositMR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDepositMR.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblDepositMR.Location = New System.Drawing.Point(710, 218)
        Me.lblDepositMR.Name = "lblDepositMR"
        Me.lblDepositMR.Size = New System.Drawing.Size(100, 30)
        Me.lblDepositMR.TabIndex = 60
        Me.lblDepositMR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDepositSR
        '
        Me.lblDepositSR.BackColor = System.Drawing.Color.White
        Me.lblDepositSR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDepositSR.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblDepositSR.Location = New System.Drawing.Point(611, 218)
        Me.lblDepositSR.Name = "lblDepositSR"
        Me.lblDepositSR.Size = New System.Drawing.Size(100, 30)
        Me.lblDepositSR.TabIndex = 59
        Me.lblDepositSR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNextDayLR
        '
        Me.lblNextDayLR.BackColor = System.Drawing.Color.White
        Me.lblNextDayLR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNextDayLR.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblNextDayLR.Location = New System.Drawing.Point(809, 370)
        Me.lblNextDayLR.Name = "lblNextDayLR"
        Me.lblNextDayLR.Size = New System.Drawing.Size(100, 30)
        Me.lblNextDayLR.TabIndex = 68
        Me.lblNextDayLR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNextDayMR
        '
        Me.lblNextDayMR.BackColor = System.Drawing.Color.White
        Me.lblNextDayMR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNextDayMR.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblNextDayMR.Location = New System.Drawing.Point(710, 370)
        Me.lblNextDayMR.Name = "lblNextDayMR"
        Me.lblNextDayMR.Size = New System.Drawing.Size(100, 30)
        Me.lblNextDayMR.TabIndex = 67
        Me.lblNextDayMR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNextDaySR
        '
        Me.lblNextDaySR.BackColor = System.Drawing.Color.White
        Me.lblNextDaySR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblNextDaySR.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblNextDaySR.Location = New System.Drawing.Point(611, 370)
        Me.lblNextDaySR.Name = "lblNextDaySR"
        Me.lblNextDaySR.Size = New System.Drawing.Size(100, 30)
        Me.lblNextDaySR.TabIndex = 66
        Me.lblNextDaySR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.White
        Me.Label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label12.Location = New System.Drawing.Point(611, 312)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(298, 30)
        Me.Label12.TabIndex = 65
        Me.Label12.Text = "Next Day"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label13
        '
        Me.Label13.BackColor = System.Drawing.Color.White
        Me.Label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label13.Location = New System.Drawing.Point(809, 341)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 30)
        Me.Label13.TabIndex = 64
        Me.Label13.Text = "LR"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.White
        Me.Label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label14.Location = New System.Drawing.Point(710, 341)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(100, 30)
        Me.Label14.TabIndex = 63
        Me.Label14.Text = "MR"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.BackColor = System.Drawing.Color.White
        Me.Label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label15.Location = New System.Drawing.Point(611, 341)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 30)
        Me.Label15.TabIndex = 62
        Me.Label15.Text = "SR"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmSC_SyncMasterData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(97, Byte), Integer), CType(CType(78, Byte), Integer), CType(CType(72, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1024, 768)
        Me.Controls.Add(Me.lblNextDayLR)
        Me.Controls.Add(Me.lblNextDayMR)
        Me.Controls.Add(Me.lblNextDaySR)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.lblDepositLR)
        Me.Controls.Add(Me.lblDepositMR)
        Me.Controls.Add(Me.lblDepositSR)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.pnSave)
        Me.Controls.Add(Me.flpServiceRateHour)
        Me.Controls.Add(Me.lblHeader)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmSC_SyncMasterData"
        Me.Text = "frmSC_FillPaper"
        Me.btnClose.ResumeLayout(False)
        Me.pnSave.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lblHeader As Label
    Friend WithEvents flpServiceRateHour As FlowLayoutPanel
    Friend WithEvents btnClose As Panel
    Friend WithEvents lblClose As Label
    Friend WithEvents pnSave As Panel
    Friend WithEvents lblSave As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents lblDepositLR As Label
    Friend WithEvents lblDepositMR As Label
    Friend WithEvents lblDepositSR As Label
    Friend WithEvents lblNextDayLR As Label
    Friend WithEvents lblNextDayMR As Label
    Friend WithEvents lblNextDaySR As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
End Class
