﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmDepositSetPINCode
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TimerTimeOut = New System.Windows.Forms.Timer(Me.components)
        Me.btnOK = New System.Windows.Forms.PictureBox()
        Me.btn0 = New System.Windows.Forms.PictureBox()
        Me.btnClear = New System.Windows.Forms.PictureBox()
        Me.btn9 = New System.Windows.Forms.PictureBox()
        Me.btn8 = New System.Windows.Forms.PictureBox()
        Me.btn7 = New System.Windows.Forms.PictureBox()
        Me.btn6 = New System.Windows.Forms.PictureBox()
        Me.btn5 = New System.Windows.Forms.PictureBox()
        Me.btn4 = New System.Windows.Forms.PictureBox()
        Me.btn3 = New System.Windows.Forms.PictureBox()
        Me.btn2 = New System.Windows.Forms.PictureBox()
        Me.btn1 = New System.Windows.Forms.PictureBox()
        Me.pnlNumPad = New System.Windows.Forms.Panel()
        Me.lblOK = New System.Windows.Forms.Label()
        Me.lblClear = New System.Windows.Forms.Label()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtPincode = New System.Windows.Forms.TextBox()
        CType(Me.btnOK, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnClear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btn1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlNumPad.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TimerTimeOut
        '
        Me.TimerTimeOut.Interval = 1000
        '
        'btnOK
        '
        Me.btnOK.BackColor = System.Drawing.Color.Transparent
        Me.btnOK.Location = New System.Drawing.Point(456, 92)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(119, 76)
        Me.btnOK.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnOK.TabIndex = 105
        Me.btnOK.TabStop = False
        '
        'btn0
        '
        Me.btn0.BackColor = System.Drawing.Color.Transparent
        Me.btn0.Location = New System.Drawing.Point(366, 93)
        Me.btn0.Name = "btn0"
        Me.btn0.Size = New System.Drawing.Size(75, 75)
        Me.btn0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn0.TabIndex = 104
        Me.btn0.TabStop = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.Color.Transparent
        Me.btnClear.Location = New System.Drawing.Point(456, 0)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(119, 75)
        Me.btnClear.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnClear.TabIndex = 103
        Me.btnClear.TabStop = False
        '
        'btn9
        '
        Me.btn9.BackColor = System.Drawing.Color.Transparent
        Me.btn9.Location = New System.Drawing.Point(274, 93)
        Me.btn9.Name = "btn9"
        Me.btn9.Size = New System.Drawing.Size(75, 75)
        Me.btn9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn9.TabIndex = 102
        Me.btn9.TabStop = False
        '
        'btn8
        '
        Me.btn8.BackColor = System.Drawing.Color.Transparent
        Me.btn8.Location = New System.Drawing.Point(183, 93)
        Me.btn8.Name = "btn8"
        Me.btn8.Size = New System.Drawing.Size(75, 75)
        Me.btn8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn8.TabIndex = 101
        Me.btn8.TabStop = False
        '
        'btn7
        '
        Me.btn7.BackColor = System.Drawing.Color.Transparent
        Me.btn7.Location = New System.Drawing.Point(92, 93)
        Me.btn7.Name = "btn7"
        Me.btn7.Size = New System.Drawing.Size(75, 75)
        Me.btn7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn7.TabIndex = 100
        Me.btn7.TabStop = False
        '
        'btn6
        '
        Me.btn6.BackColor = System.Drawing.Color.Transparent
        Me.btn6.Location = New System.Drawing.Point(2, 93)
        Me.btn6.Name = "btn6"
        Me.btn6.Size = New System.Drawing.Size(75, 75)
        Me.btn6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn6.TabIndex = 99
        Me.btn6.TabStop = False
        '
        'btn5
        '
        Me.btn5.BackColor = System.Drawing.Color.Transparent
        Me.btn5.Location = New System.Drawing.Point(366, 0)
        Me.btn5.Name = "btn5"
        Me.btn5.Size = New System.Drawing.Size(75, 75)
        Me.btn5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn5.TabIndex = 98
        Me.btn5.TabStop = False
        '
        'btn4
        '
        Me.btn4.BackColor = System.Drawing.Color.Transparent
        Me.btn4.Location = New System.Drawing.Point(274, 0)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(75, 75)
        Me.btn4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn4.TabIndex = 97
        Me.btn4.TabStop = False
        '
        'btn3
        '
        Me.btn3.BackColor = System.Drawing.Color.Transparent
        Me.btn3.Location = New System.Drawing.Point(183, 0)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(75, 75)
        Me.btn3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn3.TabIndex = 96
        Me.btn3.TabStop = False
        '
        'btn2
        '
        Me.btn2.BackColor = System.Drawing.Color.Transparent
        Me.btn2.Location = New System.Drawing.Point(92, 0)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(75, 75)
        Me.btn2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn2.TabIndex = 95
        Me.btn2.TabStop = False
        '
        'btn1
        '
        Me.btn1.BackColor = System.Drawing.Color.Transparent
        Me.btn1.Location = New System.Drawing.Point(1, 0)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(75, 75)
        Me.btn1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btn1.TabIndex = 94
        Me.btn1.TabStop = False
        '
        'pnlNumPad
        '
        Me.pnlNumPad.BackgroundImage = Global.Kiosk_Locker.My.Resources.Resources.imgNumPad
        Me.pnlNumPad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pnlNumPad.Controls.Add(Me.lblOK)
        Me.pnlNumPad.Controls.Add(Me.lblClear)
        Me.pnlNumPad.Controls.Add(Me.btn1)
        Me.pnlNumPad.Controls.Add(Me.btn2)
        Me.pnlNumPad.Controls.Add(Me.btn3)
        Me.pnlNumPad.Controls.Add(Me.btn4)
        Me.pnlNumPad.Controls.Add(Me.btn5)
        Me.pnlNumPad.Controls.Add(Me.btn6)
        Me.pnlNumPad.Controls.Add(Me.btn7)
        Me.pnlNumPad.Controls.Add(Me.btnOK)
        Me.pnlNumPad.Controls.Add(Me.btn8)
        Me.pnlNumPad.Controls.Add(Me.btn0)
        Me.pnlNumPad.Controls.Add(Me.btn9)
        Me.pnlNumPad.Controls.Add(Me.btnClear)
        Me.pnlNumPad.Location = New System.Drawing.Point(216, 275)
        Me.pnlNumPad.Name = "pnlNumPad"
        Me.pnlNumPad.Size = New System.Drawing.Size(575, 169)
        Me.pnlNumPad.TabIndex = 115
        '
        'lblOK
        '
        Me.lblOK.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblOK.BackColor = System.Drawing.Color.Transparent
        Me.lblOK.Font = New System.Drawing.Font("Thai Sans Lite", 30.0!, System.Drawing.FontStyle.Bold)
        Me.lblOK.ForeColor = System.Drawing.Color.White
        Me.lblOK.Location = New System.Drawing.Point(448, 102)
        Me.lblOK.Margin = New System.Windows.Forms.Padding(0)
        Me.lblOK.Name = "lblOK"
        Me.lblOK.Size = New System.Drawing.Size(139, 50)
        Me.lblOK.TabIndex = 118
        Me.lblOK.Text = "Confirm"
        Me.lblOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblClear
        '
        Me.lblClear.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblClear.BackColor = System.Drawing.Color.Transparent
        Me.lblClear.Font = New System.Drawing.Font("Thai Sans Lite", 30.0!, System.Drawing.FontStyle.Bold)
        Me.lblClear.ForeColor = System.Drawing.Color.White
        Me.lblClear.Location = New System.Drawing.Point(449, 9)
        Me.lblClear.Name = "lblClear"
        Me.lblClear.Size = New System.Drawing.Size(134, 50)
        Me.lblClear.TabIndex = 117
        Me.lblClear.Text = "Delete"
        Me.lblClear.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTitle
        '
        Me.lblTitle.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblTitle.Font = New System.Drawing.Font("Thai Sans Lite", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(12, 38)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(1000, 82)
        Me.lblTitle.TabIndex = 106
        Me.lblTitle.Text = "กำหนดรหัสผ่าน 6 หลัก"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackgroundImage = Global.Kiosk_Locker.My.Resources.Resources.txtBgPincode
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.txtPincode)
        Me.Panel1.Location = New System.Drawing.Point(273, 155)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(468, 63)
        Me.Panel1.TabIndex = 116
        '
        'txtPincode
        '
        Me.txtPincode.BackColor = System.Drawing.Color.FromArgb(CType(CType(152, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.txtPincode.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtPincode.Font = New System.Drawing.Font("Thai Sans Lite", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPincode.Location = New System.Drawing.Point(135, 6)
        Me.txtPincode.Multiline = True
        Me.txtPincode.Name = "txtPincode"
        Me.txtPincode.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPincode.ReadOnly = True
        Me.txtPincode.Size = New System.Drawing.Size(209, 49)
        Me.txtPincode.TabIndex = 0
        Me.txtPincode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'frmDepositSetPINCode
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1024, 553)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.pnlNumPad)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmDepositSetPINCode"
        CType(Me.btnOK, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnClear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btn1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlNumPad.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TimerTimeOut As Timer
    Friend WithEvents btn1 As PictureBox
    Friend WithEvents btn2 As PictureBox
    Friend WithEvents btn3 As PictureBox
    Friend WithEvents btn6 As PictureBox
    Friend WithEvents btn5 As PictureBox
    Friend WithEvents btn4 As PictureBox
    Friend WithEvents btn9 As PictureBox
    Friend WithEvents btn8 As PictureBox
    Friend WithEvents btn7 As PictureBox
    Friend WithEvents btnOK As PictureBox
    Friend WithEvents btn0 As PictureBox
    Friend WithEvents btnClear As PictureBox
    Friend WithEvents pnlNumPad As Panel
    Friend WithEvents lblTitle As Label
    Friend WithEvents lblOK As Label
    Friend WithEvents lblClear As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents txtPincode As TextBox
End Class
