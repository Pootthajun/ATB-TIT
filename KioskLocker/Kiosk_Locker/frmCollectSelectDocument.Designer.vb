﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCollectSelectDocument
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblOpenMethodTitle = New System.Windows.Forms.Label()
        Me.TimerTimeOut = New System.Windows.Forms.Timer(Me.components)
        Me.lblQRCodeNotification = New System.Windows.Forms.Label()
        Me.pnlPassword = New System.Windows.Forms.Panel()
        Me.lblPassword = New System.Windows.Forms.Label()
        Me.pnlQRCode = New System.Windows.Forms.Panel()
        Me.lblQRCode = New System.Windows.Forms.Label()
        Me.pnlPassword.SuspendLayout()
        Me.pnlQRCode.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblOpenMethodTitle
        '
        Me.lblOpenMethodTitle.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblOpenMethodTitle.Font = New System.Drawing.Font("Thai Sans Lite", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblOpenMethodTitle.Location = New System.Drawing.Point(12, 38)
        Me.lblOpenMethodTitle.Name = "lblOpenMethodTitle"
        Me.lblOpenMethodTitle.Size = New System.Drawing.Size(1000, 82)
        Me.lblOpenMethodTitle.TabIndex = 13
        Me.lblOpenMethodTitle.Text = "เลือกวิธีเปิดตู้ล็อกเกอร์"
        Me.lblOpenMethodTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TimerTimeOut
        '
        Me.TimerTimeOut.Enabled = True
        Me.TimerTimeOut.Interval = 1000
        '
        'lblQRCodeNotification
        '
        Me.lblQRCodeNotification.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.lblQRCodeNotification.Font = New System.Drawing.Font("Thai Sans Lite", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblQRCodeNotification.ForeColor = System.Drawing.Color.Red
        Me.lblQRCodeNotification.Location = New System.Drawing.Point(144, 449)
        Me.lblQRCodeNotification.Name = "lblQRCodeNotification"
        Me.lblQRCodeNotification.Size = New System.Drawing.Size(340, 64)
        Me.lblQRCodeNotification.TabIndex = 95
        Me.lblQRCodeNotification.Text = "เครื่องไม่สามารถ สแกน QR Code"
        Me.lblQRCodeNotification.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblQRCodeNotification.Visible = False
        '
        'pnlPassword
        '
        Me.pnlPassword.BackgroundImage = Global.Kiosk_Locker.My.Resources.Resources.iconPickupPincode
        Me.pnlPassword.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pnlPassword.Controls.Add(Me.lblPassword)
        Me.pnlPassword.Location = New System.Drawing.Point(548, 166)
        Me.pnlPassword.Name = "pnlPassword"
        Me.pnlPassword.Size = New System.Drawing.Size(280, 280)
        Me.pnlPassword.TabIndex = 15
        '
        'lblPassword
        '
        Me.lblPassword.BackColor = System.Drawing.Color.Transparent
        Me.lblPassword.Font = New System.Drawing.Font("Thai Sans Lite", 32.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblPassword.Location = New System.Drawing.Point(0, 199)
        Me.lblPassword.Margin = New System.Windows.Forms.Padding(0)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(280, 50)
        Me.lblPassword.TabIndex = 20
        Me.lblPassword.Text = "รหัสผ่าน"
        Me.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlQRCode
        '
        Me.pnlQRCode.BackgroundImage = Global.Kiosk_Locker.My.Resources.Resources.IconPickupQRCode
        Me.pnlQRCode.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pnlQRCode.Controls.Add(Me.lblQRCode)
        Me.pnlQRCode.Location = New System.Drawing.Point(173, 166)
        Me.pnlQRCode.Name = "pnlQRCode"
        Me.pnlQRCode.Size = New System.Drawing.Size(280, 280)
        Me.pnlQRCode.TabIndex = 14
        '
        'lblQRCode
        '
        Me.lblQRCode.BackColor = System.Drawing.Color.Transparent
        Me.lblQRCode.Font = New System.Drawing.Font("Thai Sans Lite", 32.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblQRCode.Location = New System.Drawing.Point(0, 199)
        Me.lblQRCode.Margin = New System.Windows.Forms.Padding(0)
        Me.lblQRCode.Name = "lblQRCode"
        Me.lblQRCode.Size = New System.Drawing.Size(280, 50)
        Me.lblQRCode.TabIndex = 18
        Me.lblQRCode.Text = "QR Code"
        Me.lblQRCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmCollectSelectDocument
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1024, 553)
        Me.Controls.Add(Me.lblQRCodeNotification)
        Me.Controls.Add(Me.pnlPassword)
        Me.Controls.Add(Me.pnlQRCode)
        Me.Controls.Add(Me.lblOpenMethodTitle)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmCollectSelectDocument"
        Me.pnlPassword.ResumeLayout(False)
        Me.pnlQRCode.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblOpenMethodTitle As Label
    Friend WithEvents TimerTimeOut As Timer
    Friend WithEvents pnlQRCode As Panel
    Friend WithEvents pnlPassword As Panel
    Friend WithEvents lblPassword As Label
    Friend WithEvents lblQRCode As Label
    Friend WithEvents lblQRCodeNotification As Label
End Class
