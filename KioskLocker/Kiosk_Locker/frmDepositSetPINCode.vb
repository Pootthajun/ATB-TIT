﻿Imports System.Data.SqlClient
Imports Kiosk_Locker.Data.KioskConfigData
Imports Kiosk_Locker.Data
Imports KioskLinqDB.ConnectDB
Imports KioskLinqDB.TABLE
Public Class frmDepositSetPINCode
    Dim TimeOut As Int32 = KioskConfig.TimeOutSec
    Dim TimeOutCheckTime As DateTime = DateTime.Now
    Public CurrentNotificationId As Long = 0

    Private Sub frmDepositSetPINCode_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.ControlBox = False
        Me.BackColor = bgColor
        If ServiceID = ConstantsData.TransactionType.CollectBelonging Then
            KioskConfig.SelectForm = KioskLockerForm.CollectByPassword
        Else
            KioskConfig.SelectForm = KioskLockerForm.DepositSetPinCode
        End If

        CurrentNotificationId = 0
        SetChildFormLanguage()
    End Sub
    Private Sub frmDepositSetPINCode_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Me.WindowState = FormWindowState.Maximized
        frmMain.CloseAllChildForm(Me)
        frmMain.pnlFooter.Visible = True
        frmMain.pnlCancel.Visible = True

        Application.DoEvents()
        If ServiceID = ConstantsData.TransactionType.CollectBelonging Then
            InsertLogTransactionActivity(Customer.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.CollectByPassword_OpenForm, "", False)
        Else
            InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositSetPinCode_OpenForm, "", False)
        End If

        TimeOutCheckTime = DateTime.Now
        TimerTimeOut.Enabled = True

    End Sub


    Private Sub TimerTimeOut_Tick(sender As Object, e As EventArgs) Handles TimerTimeOut.Tick
        If KioskConfig.SelectForm = KioskLockerForm.DepositSetPinCode Or KioskConfig.SelectForm = KioskLockerForm.CollectByPassword Then
            Application.DoEvents()
            'lblTimeOut.Text = TimeOut
            If TimeOutCheckTime.AddSeconds(TimeOut) <= DateTime.Now Then
                TimerTimeOut.Enabled = False
                TimerTimeOut.Stop()
                If ServiceID = ConstantsData.TransactionType.CollectBelonging Then
                    UpdateDepositStatus(Collect.CollectTransactionID, CollectTransactionData.TransactionStatus.TimeOut, KioskLockerStep.CollectByPassword_TimeOut)
                    InsertLogTransactionActivity(Customer.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.DepositSetPinCode_Timeout, "", False)
                Else
                    UpdateDepositStatus(Customer.ServiceTransactionID, DepositTransactionData.TransactionStatus.TimeOut, KioskLockerStep.DepositSetPinCode_Timeout)
                    InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositSetPinCode_Timeout, "", False)
                End If

                CurrentNotificationId = 0
                frmMain.CloseAllChildForm()
                Dim f As New frmHome
                f.MdiParent = frmMain
                f.Show()
            End If
        End If
    End Sub

    Private Sub DepositClearAndConfirmPin()
        If Customer.PinCode = "" Then
            Customer.PinCode = txtPincode.Text
            TmpPinCode = ""
            txtPincode.Text = ""
            lblTitle.Text = GetNotificationText(8)
            lblTitle.ForeColor = Color.Black
            CurrentNotificationId = 8
        Else
            If Customer.PinCode = txtPincode.Text Then
                frmLoading.Show(frmMain)
                Application.DoEvents()
                'ไปหน้าจอชำระเงินโลด
                InsertLogTransactionActivity(Customer.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.DepositSetPinCode_ConfirmPinCodeSuccess, "", False)
                Me.Close()

                frmDepositPayment.MdiParent = frmMain
                frmDepositPayment.Show()
            Else
                'ยืนยันรหัสส่วนตัวไม่ตรงกัน ให้เริ่มขั้นตอนใหม่
                TimeOutCheckTime = DateTime.Now
                Customer.PinCode = ""
                TmpPinCode = ""
                txtPincode.Text = ""

                InsertLogTransactionActivity(Customer.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.DepositSetPinCode_ConfirmPinCodeFail, "", False)
                'ShowDialogErrorMessage(String.Format("คุณยืนยันรหัสส่วนตัวไม่ถูกต้อง กรุณากำหนดรหัสส่วนตัว {0} หลัก", KioskConfig.PincodeLen))
                lblTitle.Text = GetNotificationText(9)
                lblTitle.ForeColor = Color.Red
                CurrentNotificationId = 9
            End If
        End If
        Application.DoEvents()
    End Sub

    Private Function CheckDataPassword() As Boolean
        Dim ret As Boolean = False
        Try
            Dim sql As String = "select t.id, t.trans_no, t.ms_locker_id, l.locker_name, l.pin_solenoid, l.pin_led, l.pin_sensor,  "
            sql += " t.service_rate, t.service_rate_limit_day, t.deposit_amt, t.paid_time, c.ms_cabinet_model_id "
            sql += " from TB_SERVICE_TRANSACTION t"
            sql += " inner join MS_LOCKER l on l.id=t.ms_locker_id"
            sql += " inner join MS_CABINET c on c.id=l.ms_cabinet_id"
            sql += " where t.ms_locker_id=@_LOCKER_ID and t.pin_code=@_PIN_CODE "
            sql += " and t.trans_status=@_TRANS_STATUS"
            sql += " and t.paid_time is not null "

            Dim p(3) As SqlParameter
            p(0) = KioskDB.SetBigInt("@_LOCKER_ID", Collect.LockerID)
            p(1) = KioskDB.SetText("@_PIN_CODE", txtPincode.Text)
            p(2) = KioskDB.SetText("@_TRANS_STATUS", Convert.ToInt16(DepositTransactionData.TransactionStatus.Success))

            Dim dt As DataTable = KioskDB.ExecuteTable(sql, p)
            If dt.Rows.Count > 0 Then
                Collect.DepositTransNo = dt.Rows(0)("trans_no")
                'กรณีพบข้อมูล ให้ตรวจสอบจะต้องไม่มีรายการรับคืนที่ Success แล้ว
                sql = "select top 1 id "
                sql += " from TB_PICKUP_TRANSACTION "
                sql += " where deposit_trans_no=@_DEPOSIT_TRANS_NO "
                sql += " and trans_status=@_PICKUP_TRANS_STATUS "

                ReDim p(2)
                p(0) = KioskDB.SetText("@_DEPOSIT_TRANS_NO", Collect.DepositTransNo)
                p(1) = KioskDB.SetText("@_PICKUP_TRANS_STATUS", Convert.ToInt16(CollectTransactionData.TransactionStatus.Success))

                Dim pDt As DataTable = KioskDB.ExecuteTable(sql, p)
                If pDt.Rows.Count = 0 Then
                    Dim dr As DataRow = dt.Rows(0)
                    ret = SetPickupInitialInformation(dr, KioskLockerStep.CollectByPassword_CheckDataPassword)
                Else
                    ret = False
                End If
                pDt.Dispose()
            Else
                '#################################################################################
                'ถ้าไม่เจอให้หาจาก Service Transaction ที่มี Status Inprogress และมี deposit_trans_no ตรงกัน
                '#################################################################################
                sql = "select t.id, t.trans_no, t.ms_locker_id, l.locker_name, l.pin_solenoid, l.pin_led, l.pin_sensor,  "
                sql += " t.service_rate, t.service_rate_limit_day, t.deposit_amt, t.paid_time, c.ms_cabinet_model_id "
                sql += " from TB_SERVICE_TRANSACTION t"
                sql += " inner join MS_LOCKER l on l.id=t.ms_locker_id"
                sql += " inner join MS_CABINET c on c.id=l.ms_cabinet_id"
                sql += " where t.ms_locker_id=@_LOCKER_ID and t.pin_code=@_PIN_CODE "
                sql += " and t.trans_status=@_TRANS_STATUS"

                ReDim p(3)
                p(0) = KioskDB.SetBigInt("@_LOCKER_ID", Collect.LockerID)
                p(1) = KioskDB.SetText("@_PIN_CODE", txtPincode.Text)
                p(2) = KioskDB.SetText("@_TRANS_STATUS", Convert.ToInt16(DepositTransactionData.TransactionStatus.Inprogress))

                dt = KioskDB.ExecuteTable(sql, p)
                If dt.Rows.Count > 0 Then
                    Collect.DepositTransNo = dt.Rows(0)("trans_no")

                    Dim dr As DataRow = dt.Rows(0)
                    ret = SetPickupInitialInformation(dr, KioskLockerStep.CollectByPassword_CheckDataPassword)
                    If ret = True Then
                        Dim lnq As TbServiceTransactionKioskLinqDB = UpdateServiceTransactionKiosk(Collect.DepositTransNo, KioskLockerStep.CollectByPassword_CheckDataPassword)
                        If lnq.ID > 0 Then
                            If lnq.PAID_TIME.Value.Year <> 1 Then
                                Collect.DepositPaidTime = lnq.PAID_TIME
                            Else
                                Collect.DepositPaidTime = lnq.TRANS_END_TIME
                            End If
                        Else
                            ret = False
                        End If
                        lnq = Nothing
                    End If
                Else
                    '#################################################################################
                    'ถ้าไม่เจออีก ให้หาจาก Service Transaction ที่ Server เลยโลด
                    '#################################################################################
                    ReDim p(2)
                    p(0) = KioskDB.SetBigInt("@_LOCKER_ID", Collect.LockerID)
                    p(1) = KioskDB.SetText("@_PIN_CODE", txtPincode.Text)

                    dt = ServerLinqDB.ConnectDB.ServerDB.ExecuteTable(sql, p)
                    If dt.Rows.Count > 0 Then
                        Collect.DepositTransNo = dt.Rows(0)("trans_no")

                        Dim dr As DataRow = dt.Rows(0)
                        ret = SetPickupInitialInformation(dr, KioskLockerStep.CollectByPassword_CheckDataPassword)
                        If ret = True Then
                            Dim lnq As ServerLinqDB.TABLE.TbServiceTransactionServerLinqDB = UpdateServiceTransactionServer(Collect.DepositTransNo, KioskLockerStep.CollectByPassword_CheckDataPassword)
                            If lnq.ID > 0 Then
                                If lnq.PAID_TIME.Value.Year <> 1 Then
                                    Collect.DepositPaidTime = lnq.PAID_TIME
                                Else
                                    Collect.DepositPaidTime = lnq.TRANS_END_TIME
                                End If
                            Else
                                ret = False
                            End If
                            lnq = Nothing
                        End If
                    End If
                End If
            End If
            dt.Dispose()

        Catch ex As Exception
            ret = False
        End Try
        Return ret
    End Function

    Private Sub CollectConfirmPincode()
        'ตรวจสอบ Pincode กรณีรับคืน
        frmLoading.Show(frmMain)
        Application.DoEvents()
        If CheckDataPassword() = True Then
            InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.CollectByPassword_CalServiceAmount, "", False)

            Collect.PickupTime = DateTime.Now
            Collect.ServiceAmount = PickupCalServiceAmount()   'ค่าบริการที่ระบบคำนวณได้
            Collect.FineAmount = PickupGetFineAmount(Collect.DepositTransNo)
            'Collect.LostQRCode = "N"
            UpdateCollectTransaction(Collect)
            Me.Close()

            Application.DoEvents()
            frmDepositPayment.MdiParent = frmMain
            frmDepositPayment.Show()
        Else
            TmpPinCode = ""
            txtPincode.Text = ""
            lblTitle.Text = GetNotificationText(10)
            lblTitle.ForeColor = Color.Red
            CurrentNotificationId = 10
            txtPincode.Text = ""
            frmLoading.Close()
        End If
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click, lblOK.Click
        If txtPincode.Text.Length <> KioskConfig.PincodeLen Then
            'ShowDialogErrorMessage(String.Format("กรุณากำหนดรหัสส่วนตัว {0} หลัก", KioskConfig.PincodeLen))
            Exit Sub
        End If
        If ServiceID = ConstantsData.TransactionType.CollectBelonging Then
            CollectConfirmPincode()
        Else
            DepositClearAndConfirmPin()
        End If

        TimeOutCheckTime = DateTime.Now
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click, lblClear.Click
        TmpPinCode = ""
        txtPincode.Text = ""
        TimeOutCheckTime = DateTime.Now
    End Sub


#Region "Fill in PIN CODE"

    Dim TmpPinCode As String = ""
    Private Sub InsertNumber(ByVal Num As Int16)
        If TmpPinCode.Length >= KioskConfig.PincodeLen Then Exit Sub

        TmpPinCode = TmpPinCode & Num
        TimeOutCheckTime = DateTime.Now
        txtPincode.Text += Num.ToString
        Application.DoEvents()
    End Sub
    Private Sub btn0_Click(sender As Object, e As EventArgs) Handles btn0.Click
        InsertNumber(0)
    End Sub
    Private Sub btn1_Click(sender As Object, e As EventArgs) Handles btn1.Click
        InsertNumber(1)
    End Sub

    Private Sub btn2_Click(sender As Object, e As EventArgs) Handles btn2.Click
        InsertNumber(2)
    End Sub

    Private Sub btn3_Click(sender As Object, e As EventArgs) Handles btn3.Click
        InsertNumber(3)
    End Sub

    Private Sub btn4_Click(sender As Object, e As EventArgs) Handles btn4.Click
        InsertNumber(4)
    End Sub

    Private Sub btn5_Click(sender As Object, e As EventArgs) Handles btn5.Click
        InsertNumber(5)
    End Sub

    Private Sub btn6_Click(sender As Object, e As EventArgs) Handles btn6.Click
        InsertNumber(6)
    End Sub

    Private Sub btn7_Click(sender As Object, e As EventArgs) Handles btn7.Click
        InsertNumber(7)
    End Sub

    Private Sub btn8_Click(sender As Object, e As EventArgs) Handles btn8.Click
        InsertNumber(8)
    End Sub

    Private Sub btn9_Click(sender As Object, e As EventArgs) Handles btn9.Click
        InsertNumber(9)
    End Sub



    Private Sub btn_MouseDown(sender As Object, e As MouseEventArgs) Handles btn0.MouseDown, btn1.MouseDown, btn2.MouseDown, btn3.MouseDown, btn4.MouseDown, btn5.MouseDown, btn6.MouseDown, btn7.MouseDown, btn8.MouseDown, btn9.MouseDown
        Dim btn As PictureBox = sender
        btn.BackColor = Color.Gray
    End Sub

    Private Sub btn_MouseUp(sender As Object, e As MouseEventArgs) Handles btn0.MouseUp, btn1.MouseUp, btn2.MouseUp, btn3.MouseUp, btn4.MouseUp, btn5.MouseUp, btn6.MouseUp, btn7.MouseUp, btn8.MouseUp, btn9.MouseUp
        Dim btn As PictureBox = sender
        btn.BackColor = Color.Transparent
    End Sub
#End Region

End Class