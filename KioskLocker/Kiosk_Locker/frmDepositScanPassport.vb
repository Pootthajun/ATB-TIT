﻿Imports System.IO
Imports Kiosk_Locker.Data
Imports System.Data.SqlClient
Imports Kiosk_Locker.Data.KioskConfigData
Imports ThaiNationalIDCard
Imports KioskLinqDB.ConnectDB
Imports KioskLinqDB.TABLE

Public Class frmDepositScanPassport
    Dim TimeOut As Int32 = KioskConfig.TimeOutSec
    Dim TimeOutCheckTime As DateTime = DateTime.Now


    Private Delegate Sub myDelegate(data As String)
    Private myForm As myDelegate
    Private ReadProcessing As myDelegate

    Private Sub frmDepositPassport_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.ControlBox = False
        Me.BackColor = bgColor
        Me.WindowState = FormWindowState.Maximized

        If ServiceID = ConstantsData.TransactionType.CollectBelonging Then
            KioskConfig.SelectForm = KioskLockerForm.CollectScanPersonInfo
        Else
            KioskConfig.SelectForm = KioskLockerForm.DepositScanPersonInfo
        End If
        SetChildFormLanguage()
        RemoveHandler PassportScanner.ReceiveEvent, AddressOf PassportScanDataReceived
    End Sub


    Public Sub StartInitialDevice()
        Try
            If PassportScanner.StartPassportDevice(KioskConfig.PassportDeviceName) = True Then
                'ตรวจสอบสถานะการทำงานของเครื่องอ่าน Passport
                If PassportScanner.ConnectPassportDevice(KioskConfig.IDCardPassportVID) = True Then
                    If ServiceID = TransactionType.DepositBelonging Then
                        InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckPassportDevice, "", False)
                    ElseIf ServiceID = TransactionType.CollectBelonging Then
                        InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_CheckPassportDevice, "", False)
                    End If

                    AddHandler PassportScanner.StartWaitingProgress, AddressOf PassportWaitingProgress

                    If IdcardScanner.MonitorStart(KioskConfig.IDCardNoDeviceName) = True Then
                        If ServiceID = TransactionType.DepositBelonging Then
                            InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckIDCardDevice, "", False)
                        ElseIf ServiceID = TransactionType.CollectBelonging Then
                            InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_CheckIDCardDevice, "", False)
                        End If

                        'AddHandler IdcardScanner.eventCardInserted, AddressOf IdCardScanDataReceived
                        AddHandler IdcardScanner.eventCardInsertedWithPhoto, AddressOf IdCardScanDataReceived
                        AddHandler IdcardScanner.eventCardRemoved, AddressOf IdCardRemoved
                        AddHandler IdcardScanner.eventPhotoProgress, AddressOf readPhotoProgress

                        UpdateDeviceStatus(DeviceID.IDCardPassportScanner, PassportScanerStatus.Ready)
                        SendKioskAlarm("PASSPORT_SCANNER_DISCONNECTED", False)
                        TimeOutCheckTime = DateTime.Now

                    Else
                        If ServiceID = TransactionType.DepositBelonging Then
                            InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckIDCardDevice, " ไม่สามารถใช้งานเครื่องอ่านบัตรประชาชนได้", True)
                            UpdateDepositStatus(Customer.ServiceTransactionID, DepositTransactionData.TransactionStatus.Problem, KioskLockerStep.DepositScanPersonInfo_CheckIDCardDevice)
                            ShowFormError("Out of Service", "เครื่องอ่านบัตรประชาชน ไม่สามารถใช้งานได้", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckIDCardDevice, True)
                        ElseIf ServiceID = TransactionType.CollectBelonging Then
                            InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_CheckIDCardDevice, " ไม่สามารถใช้งานเครื่องอ่านบัตรประชาชนได้", True)
                            UpdateCollectStatus(Collect.CollectTransactionID, CollectTransactionData.TransactionStatus.Problem, KioskLockerStep.PickupScanPersonInfo_CheckIDCardDevice)
                            ShowFormError("Out of Service", "เครื่องอ่านบัตรประชาชน ไม่สามารถใช้งานได้", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_CheckIDCardDevice, True)
                        End If

                        UpdateDeviceStatus(DeviceID.IDCardPassportScanner, PassportScanerStatus.Disconnected)
                        SendKioskAlarm("PASSPORT_SCANNER_DISCONNECTED", True)
                        Exit Sub
                    End If
                Else
                    If ServiceID = TransactionType.DepositBelonging Then
                        UpdateDepositStatus(Customer.ServiceTransactionID, DepositTransactionData.TransactionStatus.Problem, KioskLockerStep.DepositScanPersonInfo_CheckPassportDevice)
                        InsertErrorLog("เครื่องอ่านหนังสือเดินทาง ไม่สามารถใช้งานได้", Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckPassportDevice)
                        InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckPassportDevice, " ไม่สามารถใช้งานเครื่องอ่านหนังสือเดินทางได้", True)
                    ElseIf ServiceID = TransactionType.CollectBelonging Then

                        UpdateCollectStatus(Collect.CollectTransactionID, CollectTransactionData.TransactionStatus.Problem, KioskLockerStep.PickupScanPersonInfo_CheckPassportDevice)
                        InsertErrorLog("เครื่องอ่านหนังสือเดินทาง ไม่สามารถใช้งานได้", "", Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_CheckPassportDevice)
                        InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_CheckPassportDevice, " ไม่สามารถใช้งานเครื่องอ่านหนังสือเดินทางได้", True)
                    End If

                    UpdateDeviceStatus(DeviceID.IDCardPassportScanner, PassportScanerStatus.Disconnected)
                    SendKioskAlarm("PASSPORT_SCANNER_DISCONNECTED", True)
                    ShowFormError("Out of Service", "เครื่องอ่านหนังสือเดินทาง ไม่สามารถใช้งานได้", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckPassportDevice, True)
                    Exit Sub
                End If

                TimerScan.Enabled = True
                TimerTimeOut.Enabled = True
            Else
                If ServiceID = TransactionType.DepositBelonging Then
                    InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckPassportDevice, "เครื่องอ่านหนังสือเดินทาง ไม่พร้อมใช้งาน", True)
                    UpdateDepositStatus(Customer.ServiceTransactionID, DepositTransactionData.TransactionStatus.Problem, KioskLockerStep.DepositScanPersonInfo_CheckPassportDevice)
                    ShowFormError("Out of Service", "เครื่องอ่านหนังสือเดินทาง ไม่สามารถใช้งานได้", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckPassportDevice, True)
                ElseIf ServiceID = TransactionType.CollectBelonging Then
                    InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_CheckPassportDevice, "เครื่องอ่านหนังสือเดินทาง ไม่พร้อมใช้งาน", True)
                    UpdateCollectStatus(Collect.CollectTransactionID, CollectTransactionData.TransactionStatus.Problem, KioskLockerStep.PickupScanPersonInfo_CheckPassportDevice)
                    ShowFormError("Out of Service", "เครื่องอ่านหนังสือเดินทาง ไม่สามารถใช้งานได้", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_CheckPassportDevice, True)
                End If
            End If
        Catch ex As Exception
            Dim _err As String = "Exception StartInitialDevice :" & ex.Message & vbNewLine & ex.StackTrace
            If ServiceID = TransactionType.DepositBelonging Then
                InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckPassportDevice, _err, True)
                UpdateDepositStatus(Customer.ServiceTransactionID, DepositTransactionData.TransactionStatus.Problem, KioskLockerStep.DepositScanPersonInfo_CheckPassportDevice)
                ShowFormError("Out of Service", _err, KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckPassportDevice, True)
            ElseIf ServiceID = TransactionType.CollectBelonging Then
                InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_CheckPassportDevice, _err, True)
                UpdateCollectStatus(Collect.CollectTransactionID, CollectTransactionData.TransactionStatus.Problem, KioskLockerStep.PickupScanPersonInfo_CheckPassportDevice)
                ShowFormError("Out of Service", _err, KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_CheckPassportDevice, True)
            End If
        End Try
    End Sub




    Private Sub frmDepositScanPassport_Shown(sender As Object, e As EventArgs) Handles Me.Shown

        frmMain.pnlAds.Visible = False
        frmMain.pnlFooter.Visible = True
        frmMain.pnlCancel.Visible = True
        CheckForIllegalCrossThreadCalls = False
        TimeOutCheckTime = DateTime.Now

        myForm = AddressOf OpenFormPayment
        ReadProcessing = AddressOf ShowProcessing
        Application.DoEvents()

        'frmDepositPayment.MdiParent = frmMain
        If ServiceID = TransactionType.DepositBelonging Then
            InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_OpenForm, "", False)
        ElseIf ServiceID = TransactionType.CollectBelonging Then
            InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_OpenForm, "", False)
        End If

    End Sub

    Private Sub OpenFormPayment(data As String)
        Threading.Thread.Sleep(500)
        TimerScan.Enabled = False
        TimerScan.Stop()

        TimerTimeOut.Enabled = False
        TimerTimeOut.Stop()

        frmLoading.Show(frmMain)
        RemoveHandler PassportScanner.ReceiveEvent, AddressOf PassportScanDataReceived

        RemoveHandler IdcardScanner.eventCardInsertedWithPhoto, AddressOf IdCardScanDataReceived
        RemoveHandler IdcardScanner.eventCardRemoved, AddressOf IdCardRemoved
        RemoveHandler IdcardScanner.eventPhotoProgress, AddressOf readPhotoProgress

        '#######################################
        'เฉพาะกรณี QR Code หาย จะต้องทำการดึง Profile มาก่อนที่จะเข้าหน้า Payment
        Dim IsGetPickupProfile As Boolean = False
        If ServiceID = ConstantsData.TransactionType.CollectBelonging Then
            IsGetPickupProfile = GetPickupWithScanPersonInfo(data)
            Application.DoEvents()
        Else
            IsGetPickupProfile = True
        End If
        '#######################################

        If IsGetPickupProfile = True Then
            Dim IsUpdateTransactionStatus As Boolean = False
            If ServiceID = ConstantsData.TransactionType.DepositBelonging Then
                IsUpdateTransactionStatus = UpdateServiceTransaction(Customer).IsSuccess
            ElseIf ServiceID = ConstantsData.TransactionType.CollectBelonging Then
                IsUpdateTransactionStatus = UpdateCollectTransaction(Collect).IsSuccess
            End If

            'จะต้อง Update Transaction เสร็จก่อนค่อยไปหน้าถัดไป
            If IsUpdateTransactionStatus = True Then
                frmDepositPayment.MdiParent = frmMain
                frmDepositPayment.Show()
                frmLoading.Close()
                Application.DoEvents()

                Me.Close()
            Else
                lblScanFail.Visible = True
                TimerScan.Enabled = True
                TimerScan.Start()

                TimerTimeOut.Enabled = True
                TimerTimeOut.Start()
                frmLoading.Close()
                AddHandler IdcardScanner.eventCardInsertedWithPhoto, AddressOf IdCardScanDataReceived
                AddHandler IdcardScanner.eventCardRemoved, AddressOf IdCardRemoved
                AddHandler IdcardScanner.eventPhotoProgress, AddressOf readPhotoProgress
                pnlProcessing.Visible = False
                Application.DoEvents()
                InsertLogTransactionActivity(Customer.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, 0, "Error Update Customer Data", True)
            End If
        Else
            lblScanFail.Visible = True
            TimerScan.Enabled = True
            TimerScan.Start()

            TimerTimeOut.Enabled = True
            TimerTimeOut.Start()
            frmLoading.Close()
            AddHandler IdcardScanner.eventCardInsertedWithPhoto, AddressOf IdCardScanDataReceived
            AddHandler IdcardScanner.eventCardRemoved, AddressOf IdCardRemoved
            AddHandler IdcardScanner.eventPhotoProgress, AddressOf readPhotoProgress
            pnlProcessing.Visible = False
            Application.DoEvents()
        End If
    End Sub


    Private Function SetPickupInformation(dr As DataRow) As Boolean

        Collect.DepositTransNo = dr("trans_no")
        Collect.LockerID = Convert.ToInt64(dr("ms_locker_id"))
        Collect.LockerName = dr("locker_name").ToString
        If Convert.IsDBNull(dr("pin_solenoid")) = False Then Collect.LockerPinSolenoid = Convert.ToInt16(dr("pin_solenoid"))
        If Convert.IsDBNull(dr("pin_led")) = False Then Collect.LockerPinLED = Convert.ToInt16(dr("pin_led"))
        If Convert.IsDBNull(dr("pin_sensor")) = False Then Collect.LockerPinSendor = dr("pin_sensor")

        Collect.DepositAmount = Convert.ToInt64(dr("deposit_amt"))
        Collect.CabinetModelID = Convert.ToInt64(dr("ms_cabinet_model_id"))
        If Convert.IsDBNull(dr("paid_time")) = False Then
            Collect.DepositPaidTime = Convert.ToDateTime(dr("paid_time"))
        Else
            Collect.DepositPaidTime = Convert.ToDateTime(dr("trans_end_time"))
        End If

        Collect.PickupTime = DateTime.Now
        Collect.ServiceAmount = PickupCalServiceAmount()

        Dim re As ExecuteDataInfo = UpdateCollectTransaction(Collect)
        Return re.IsSuccess
    End Function

    Private Function GetPickupWithScanPersonInfo(CardType As String) As Boolean
        Dim ret As Boolean = False
        Try
            Dim p(2) As SqlParameter

            Dim sql As String = "select t.id, t.trans_no, t.ms_locker_id, l.locker_name, l.pin_solenoid, l.pin_led, l.pin_sensor,  "
            sql += " t.deposit_amt, t.paid_time, t.trans_end_time, c.ms_cabinet_model_id "
            sql += " from TB_SERVICE_TRANSACTION t"
            sql += " inner join MS_LOCKER l on l.id=t.ms_locker_id"
            sql += " inner join MS_CABINET c on c.id=l.ms_cabinet_id "
            sql += " where 1=1 "
            If CardType = "PASSPORT" Then
                InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_GetPickupWithScanPassport, "PassportNO=" & Collect.PassportNo, False)

                sql += " and t.passport_no=@_PASSPORT_NO "
                p(0) = KioskDB.SetText("@_PASSPORT_NO", Collect.PassportNo)

            ElseIf CardType = "IDCARD" Then
                InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_GetPickupWithScanIDCard, "IDCardNo=" & Collect.IDCardNo, False)

                sql += " and t.idcard_no=@_IDCARD_NO"
                p(0) = KioskDB.SetText("@_IDCARD_NO", Collect.IDCardNo)
            End If

            sql += " and t.trans_status=@_TRANS_STATUS"
            sql += " and t.paid_time is not null"
            sql += " order by t.paid_time "

            p(1) = KioskDB.SetText("@_TRANS_STATUS", Convert.ToInt16(DepositTransactionData.TransactionStatus.Success))

            Dim lnq As New TbServiceTransactionKioskLinqDB
            Dim dt As DataTable = lnq.GetListBySql(sql, Nothing, p)
            If dt.Rows.Count > 0 Then
                For Each tmpDr As DataRow In dt.Rows
                    Dim DepositTransNo As String = tmpDr("trans_no")

                    'กรณีพบข้อมูล ให้ตรวจสอบจะต้องไม่มีรายการรับคืนที่ Success แล้ว
                    sql = "select top 1 id "
                    sql += " from TB_PICKUP_TRANSACTION "
                    sql += " where deposit_trans_no=@_DEPOSIT_TRANS_NO "
                    sql += " and trans_status=@_PICKUP_TRANS_STATUS "

                    ReDim p(2)
                    p(0) = KioskDB.SetText("@_DEPOSIT_TRANS_NO", DepositTransNo)
                    p(1) = KioskDB.SetText("@_PICKUP_TRANS_STATUS", Convert.ToInt16(CollectTransactionData.TransactionStatus.Success))

                    Dim pDt As DataTable = KioskDB.ExecuteTable(sql, p)
                    If pDt.Rows.Count = 0 Then
                        InsertLogTransactionActivity(DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_HaveData, "", False)

                        ret = SetPickupInformation(tmpDr)
                        Exit For
                    Else
                        InsertLogTransactionActivity(DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_HaveData, "รายการฝาก TransNo=" & DepositTransNo & " ได้มีการรับคืนแล้ว จึงไม่สามารถทำรายการรับคืนได้อีก", False)
                        ret = False
                    End If
                    pDt.Dispose()
                Next
            Else
                InsertLogTransactionActivity("", Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_NoPersonData, "", False)

                Dim MsStepID As Long = 0

                '#################################################################################
                'ถ้าไม่เจอให้หาจาก Service Transaction ที่มี Status Inprogress และมี deposit_trans_no ตรงกัน
                '#################################################################################
                sql = "select t.id, t.trans_no, t.ms_locker_id, l.locker_name, l.pin_solenoid, l.pin_led, l.pin_sensor,  "
                sql += " t.deposit_amt, t.paid_time, c.ms_cabinet_model_id "
                sql += " from TB_SERVICE_TRANSACTION t"
                sql += " inner join MS_LOCKER l on l.id=t.ms_locker_id"
                sql += " inner join MS_CABINET c on c.id=l.ms_cabinet_id "
                sql += " where t.trans_status=0 "  'Inprogress

                Dim pp(1) As SqlParameter
                If CardType = "PASSPORT" Then
                    MsStepID = KioskLockerStep.PickupScanPersonInfo_GetPickupWithScanPassport
                    InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, MsStepID, "หารายการฝากที่มี Trans_Status=0 จาก Kiosk ด้วย PassportNO=" & Collect.PassportNo, False)

                    sql += " and t.passport_no=@_PASSPORT_NO "
                    pp(0) = KioskDB.SetText("@_PASSPORT_NO", Collect.PassportNo)
                ElseIf CardType = "IDCARD" Then
                    MsStepID = KioskLockerStep.PickupScanPersonInfo_GetPickupWithScanIDCard
                    InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, MsStepID, "หารายการฝากที่มี Trans_Status=0 จาก Kiosk ด้วย IDCardNo=" & Collect.IDCardNo, False)

                    sql += " and t.idcard_no=@_IDCARD_NO"
                    pp(0) = KioskDB.SetText("@_IDCARD_NO", Collect.IDCardNo)
                End If

                dt = KioskDB.ExecuteTable(sql, pp)
                If dt.Rows.Count > 0 Then
                    Dim dr As DataRow = dt.Rows(0)
                    ret = SetPickupInformation(dr)
                    If ret = True Then
                        Dim kLnq As TbServiceTransactionKioskLinqDB = UpdateServiceTransactionKiosk(dr("trans_no"), MsStepID)
                        If kLnq.ID > 0 Then
                            Collect.DepositPaidTime = kLnq.PAID_TIME
                        End If
                        kLnq = Nothing
                    End If
                Else
                    '#################################################################################
                    'ถ้าไม่เจออีก ให้หาจาก Service Transaction ที่ Server เลยโลด
                    '#################################################################################
                    Dim sP(1) As SqlParameter

                    If CardType = "PASSPORT" Then
                        InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, MsStepID, "หารายการฝากที่มี Trans_Status=0 จาก Server ด้วย PassportNO=" & Collect.PassportNo, False)
                        sP(0) = KioskDB.SetText("@_PASSPORT_NO", Collect.PassportNo)

                    ElseIf CardType = "IDCARD" Then
                        InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, MsStepID, "หารายการฝากที่มี Trans_Status=0 จาก Server ด้วย IDCardNo=" & Collect.IDCardNo, False)
                        sP(0) = KioskDB.SetText("@_IDCARD_NO", Collect.IDCardNo)
                    End If

                    dt = ServerLinqDB.ConnectDB.ServerDB.ExecuteTable(sql, sP)
                    If dt.Rows.Count > 0 Then
                        Dim dr As DataRow = dt.Rows(0)
                        ret = SetPickupInformation(dr)
                        If ret = True Then
                            Dim sLnq As ServerLinqDB.TABLE.TbServiceTransactionServerLinqDB = UpdateServiceTransactionServer(dr("trans_no"), MsStepID)
                            If sLnq.ID > 0 Then
                                Collect.DepositPaidTime = sLnq.PAID_TIME
                            End If
                            sLnq = Nothing
                        End If
                    End If
                End If
            End If
            dt.Dispose()
        Catch ex As Exception
            ret = False
            InsertErrorLog("Exception : " & ex.Message & " " & ex.StackTrace, Collect.DepositTransNo, Collect.TransactionNo, 0, KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_GetPickupDataFail)
        End Try

        Return ret
    End Function

    Private Sub ShowProcessing(data As String)
        If pnlProcessing.Visible = False Then
            pnlProcessing.Left = (Me.Width / 2) - (pnlProcessing.Width / 2)
            pnlProcessing.Top = 35

            pnlProcessing.Visible = True
            TimerScan.Enabled = False
            Application.DoEvents()

            If data = "IDCARD" Then
                RemoveHandler PassportScanner.ReceiveEvent, AddressOf PassportScanDataReceived
            End If
        End If
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        ShowProcessing("")
        Threading.Thread.Sleep(5000)
        '############## Debug #############
        Customer.IDCardNo = "3410101141069"
        Customer.NationCode = "THA"
        Customer.FirstName = "AKKARAWAT"
        Customer.LastName = "POOTTHAJUN"
        Customer.BirthDate = New DateTime(1982, 3, 1)
        Customer.Gender = "M"
        Customer.IDCardExpireDate = New DateTime(2021, 2, 28)
        Customer.CustomerImage = Nothing

        Collect.IDCardNo = "3410101141069"
        Collect.NationCode = "THA"
        Collect.FirstName = "AKKARAWAT"
        Collect.LastName = "POOTTHAJUN"
        Collect.BirthDate = New DateTime(1982, 3, 1)
        Collect.Gender = "M"
        Collect.IDCardExpireDate = New DateTime(2021, 2, 28)
        Collect.CustomerImage = Nothing

        OpenFormPayment("IDCARD")
    End Sub

#Region "ID Card Function"
    'Private Sub IdCardScanDataReceived(Result As ScanPassport.Data.IDCardData)
    Private Sub IdCardScanDataReceived(Result As Personal)
        Try
            TimeOutCheckTime = DateTime.Now
            TimerScan.Enabled = False
            RemoveHandler PassportScanner.ReceiveEvent, AddressOf PassportScanDataReceived

            TimerTimeOut.Enabled = False
            TimeOutCheckTime = DateTime.Now

            If ServiceID = TransactionType.DepositBelonging Then
                InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_InsertIdCard, "", False)
            ElseIf ServiceID = TransactionType.CollectBelonging Then
                InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_InsertIdCard, "", False)
            End If

            If Result.Citizenid <> "" And
                       Result.En_Firstname <> "" And
                       Result.En_Lastname <> "" And
                       Result.Birthday.Year <> 1 And
                       Result.Sex <> "" And
                        Result.Expire.Year <> 1 And
                        Result.PhotoRaw IsNot Nothing Then
                Try
                    Dim CardExpired As Boolean = False
                    'Check ID Card Expire
                    If KioskConfig.CardExpireMonth > 0 Then
                        If ServiceID = TransactionType.DepositBelonging Then
                            InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckIDCardExpire, "", False)
                        ElseIf ServiceID = TransactionType.CollectBelonging Then
                            InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_CheckIDCardExpire, "", False)
                        End If

                        If DateTime.Now.AddMonths(KioskConfig.CardExpireMonth).Date > Result.Expire Then
                            'Card Expire
                            CardExpired = True
                            lblScanFail.Visible = True
                            lblScanFail.Text = "ID Card has Expired"
                            pnlProcessing.Visible = False
                            Threading.Thread.Sleep(500)
                            TimerScan.Start()
                            TimerScan.Enabled = True

                            TimerTimeOut.Start()
                            TimerTimeOut.Enabled = True

                            If ServiceID = TransactionType.DepositBelonging Then
                                InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckIDCardExpire, "บัตรปรุะชาชนหมดอายุ", False)
                            ElseIf ServiceID = TransactionType.CollectBelonging Then
                                InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_CheckIDCardExpire, "บัตรปรุะชาชนหมดอายุ", False)
                            End If
                        End If
                    End If

                    If CardExpired = False Then
                        If ServiceID = TransactionType.DepositBelonging Then
                            Customer.IDCardNo = Result.Citizenid
                            Customer.NationCode = "THA"
                            Customer.FirstName = Result.En_Firstname
                            Customer.LastName = Result.En_Lastname
                            Customer.BirthDate = Result.Birthday
                            Customer.Gender = IIf(Result.Sex.Trim = "ชาย", "M", "F")
                            Customer.IDCardExpireDate = Result.Expire
                            Customer.CustomerImage = Result.PhotoRaw

                            InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_ScanIDCard, "CardNo=" & Result.Citizenid, False)
                        ElseIf ServiceID = TransactionType.CollectBelonging Then
                            'For Pickup
                            Collect.IDCardNo = Result.Citizenid
                            Collect.NationCode = "THA"
                            Collect.FirstName = Result.En_Firstname
                            Collect.LastName = Result.En_Lastname
                            Collect.BirthDate = Result.Birthday
                            Collect.Gender = IIf(Result.Sex.Trim = "ชาย", "M", "F")
                            Collect.PassportExpireDate = Result.Expire
                            Collect.CustomerImage = Result.PhotoRaw

                            InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_ScanIDCard, "CardNo=" & Result.Citizenid, False)
                        End If

                        Threading.Thread.Sleep(1000)
                        Me.Invoke(myForm, "IDCARD")
                    End If
                Catch ex As Exception
                    lblScanFail.Visible = True
                    pnlProcessing.Visible = False
                    Threading.Thread.Sleep(500)
                    TimerScan.Enabled = True
                    TimerScan.Start()

                    TimerTimeOut.Enabled = True
                    TimerTimeOut.Start()

                    If ServiceID = TransactionType.DepositBelonging Then
                        InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_ScanIDCard, "Exception " & ex.Message & " " & ex.StackTrace, True)
                    ElseIf ServiceID = TransactionType.CollectBelonging Then
                        InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_ScanIDCard, "Exception " & ex.Message & " " & ex.StackTrace, True)
                    End If
                    TimeOutCheckTime = DateTime.Now
                End Try

                'pnlProcessing.Visible = False
                Application.DoEvents()
            Else
                lblScanFail.Visible = True
                lblScanFail.Text = "ไม่สามารถอ่านบัตรประชาชนได้ กรุณาลองอีกครั้ง"
                pnlProcessing.Visible = False

                Threading.Thread.Sleep(500)
                TimerScan.Enabled = True
                TimerScan.Start()

                TimerTimeOut.Enabled = True
                TimerTimeOut.Start()
                TimeOutCheckTime = DateTime.Now
                pnlProcessing.Visible = False
                Application.DoEvents()
            End If
        Catch ex As Exception
            If ServiceID = TransactionType.DepositBelonging Then
                InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_ScanIDCard, "", True)
                UpdateDepositStatus(Customer.ServiceTransactionID, DepositTransactionData.TransactionStatus.Problem, KioskLockerStep.DepositScanPersonInfo_ScanIDCard)
            ElseIf ServiceID = TransactionType.CollectBelonging Then
                InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_ScanIDCard, "", True)
                UpdateCollectStatus(Collect.CollectTransactionID, CollectTransactionData.TransactionStatus.Problem, KioskLockerStep.PickupScanPersonInfo_ScanIDCard)
            End If
            TimeOutCheckTime = DateTime.Now
        End Try

    End Sub

    Private Sub IdCardRemoved()
        Dim MsSetpID As Long = 0
        If ServiceID = TransactionType.DepositBelonging Then
            InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_RemoveIdCard, "", False)
        ElseIf ServiceID = TransactionType.CollectBelonging Then
            InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_RemoveIdCard, "", False)
        End If

        If pnlProcessing.Visible = True Then
            pnlProcessing.Visible = False
            RemoveHandler PassportScanner.ReceiveEvent, AddressOf PassportScanDataReceived
            AddHandler PassportScanner.ReceiveEvent, AddressOf PassportScanDataReceived
            Application.DoEvents()
        End If

        TimeOutCheckTime = DateTime.Now

        'Customer.IDCardNo = ""
        'Customer.NationCode = ""
        'Customer.FirstName = ""
        'Customer.LastName = ""
        'Customer.BirthDate = New DateTime(1, 1, 1)
        'Customer.Gender = ""
        'Customer.IDCardExpireDate = New DateTime(1, 1, 1)
        'Customer.CustomerImage = Nothing

        ''For Pickup
        'Collect.IDCardNo = ""
        'Collect.NationCode = ""
        'Collect.FirstName = ""
        'Collect.LastName = ""
        'Collect.BirthDate = New DateTime(1, 1, 1)
        'Collect.Gender = ""
        'Collect.IDCardExpireDate = New DateTime(1, 1, 1)
        'Collect.CustomerImage = Nothing

    End Sub

    Private Sub readPhotoProgress(value As Integer, maximum As Integer)
        Try
            Me.Invoke(ReadProcessing, "IDCARD")
            Application.DoEvents()
            TimeOutCheckTime = DateTime.Now
        Catch ex As Exception

        End Try
    End Sub

#End Region


#Region "Passport Function"
    Private Sub PassportScanDataReceived(ByVal Result As ScanPassport.Data.PassportData)
        Application.DoEvents()

        Try
            TimerScan.Enabled = False
            TimerScan.Stop()

            TimerTimeOut.Enabled = False
            TimerTimeOut.Stop()

            If ServiceID = TransactionType.DepositBelonging Then
                InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_InsertPassport, "", False)
            ElseIf ServiceID = TransactionType.CollectBelonging Then
                InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_InsertPassport, "", False)
            End If

            If Result.PassportNumber <> "" And
                       Result.PersonalNumber <> "" And
                       Result.NationCode <> "" And
                       Result.FirstName <> "" And
                       Result.LastName <> "" And
                       Result.BirthDate.Year <> 1 And
                       Result.Gender <> "" And
                       Result.ExpireDate.Year <> 1 And
                       Result.PersonImage IsNot Nothing Then

                TimeOutCheckTime = DateTime.Now
                Application.DoEvents()

                Try
                    'CheckPassportExpire
                    Dim PassportExpired As Boolean = False
                    If KioskConfig.PassportExpireMonth > 0 Then
                        If ServiceID = TransactionType.DepositBelonging Then
                            InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckPassportExpire, "", False)
                        ElseIf ServiceID = TransactionType.CollectBelonging Then
                            InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_CheckPassportExpire, "", False)
                        End If

                        If DateTime.Now.AddMonths(KioskConfig.PassportExpireMonth).Date > Result.ExpireDate Then
                            'Card Expire
                            PassportExpired = True
                            lblScanFail.Visible = True
                            lblScanFail.Text = "Passport has Expired"
                            pnlProcessing.Visible = False
                            Threading.Thread.Sleep(500)
                            TimerScan.Enabled = True
                            TimerScan.Start()

                            TimerTimeOut.Enabled = True
                            TimerTimeOut.Start()

                            If ServiceID = TransactionType.DepositBelonging Then
                                InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_CheckPassportExpire, "หนังสือเดินทางหมดอายุ", False)
                            ElseIf ServiceID = TransactionType.CollectBelonging Then
                                InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_CheckPassportExpire, "หนังสือเดินทางหมดอายุ", False)
                            End If
                        End If
                    End If
                    If PassportExpired = False Then
                        'Customer for Deposit
                        Customer.PassportNo = Result.PassportNumber
                        Customer.IDCardNo = Result.PersonalNumber
                        Customer.NationCode = Result.NationCode
                        Customer.FirstName = Result.FirstName
                        Customer.LastName = Result.LastName
                        Customer.BirthDate = Result.BirthDate
                        Customer.Gender = Result.Gender
                        Customer.PassportExpireDate = Result.ExpireDate
                        Customer.CustomerImage = Result.PersonImage

                        'For Pickup
                        Collect.PassportNo = Result.PassportNumber
                        Collect.IDCardNo = Result.PersonalNumber
                        Collect.NationCode = Result.NationCode
                        Collect.FirstName = Result.FirstName
                        Collect.LastName = Result.LastName
                        Collect.BirthDate = Result.BirthDate
                        Collect.Gender = Result.Gender
                        Collect.PassportExpireDate = Result.ExpireDate
                        Collect.CustomerImage = Result.PersonImage

                        If ServiceID = TransactionType.DepositBelonging Then
                            InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_ScanPassport, "PassportNo=" & Result.PassportNumber, False)
                        ElseIf ServiceID = TransactionType.CollectBelonging Then
                            InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_ScanPassport, "PassportNo=" & Result.PassportNumber, False)
                        End If

                        Threading.Thread.Sleep(1000)
                        Me.Invoke(myForm, "PASSPORT")
                    End If
                Catch ex As Exception
                    lblScanFail.Visible = True
                    pnlProcessing.Visible = False
                    Threading.Thread.Sleep(500)
                    TimerScan.Enabled = True
                    TimerScan.Start()

                    TimerTimeOut.Enabled = True
                    TimerTimeOut.Start()

                    If ServiceID = TransactionType.DepositBelonging Then
                        InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_ScanPassport, "Exception " & ex.Message & " " & ex.StackTrace, True)
                    ElseIf ServiceID = TransactionType.CollectBelonging Then
                        InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_ScanPassport, "Exception " & ex.Message & " " & ex.StackTrace, True)
                    End If
                    pnlProcessing.Visible = False
                End Try
                Application.DoEvents()
            Else

                Threading.Thread.Sleep(500)
                TimerScan.Enabled = True
                TimerScan.Start()

                TimerTimeOut.Enabled = True
                TimerTimeOut.Start()

                pnlProcessing.Visible = False
                Application.DoEvents()
            End If
        Catch ex As Exception

            If ServiceID = TransactionType.DepositBelonging Then
                InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_ScanPassport, "", True)
                UpdateDepositStatus(Customer.ServiceTransactionID, DepositTransactionData.TransactionStatus.Problem, KioskLockerStep.DepositScanPersonInfo_ScanPassport)
            ElseIf ServiceID = TransactionType.CollectBelonging Then
                InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_ScanPassport, "", True)
                UpdateCollectStatus(Collect.CollectTransactionID, CollectTransactionData.TransactionStatus.Problem, KioskLockerStep.PickupScanPersonInfo_ScanPassport)
            End If

            pnlProcessing.Visible = False
            Application.DoEvents()
        End Try

    End Sub

    Private Sub PassportWaitingProgress()
        Try
            Me.Invoke(ReadProcessing, "PASSPORT")
            Application.DoEvents()
            TimeOutCheckTime = DateTime.Now
        Catch ex As Exception

        End Try
        Application.DoEvents()
    End Sub

    Private Sub TimerScan_Tick(sender As Object, e As EventArgs) Handles TimerScan.Tick
        RemoveHandler PassportScanner.ReceiveEvent, AddressOf PassportScanDataReceived
        AddHandler PassportScanner.ReceiveEvent, AddressOf PassportScanDataReceived
        PassportScanner.StartScan()
        Application.DoEvents()
    End Sub

    Private Sub frmDepositScanPassport_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try

            IdcardScanner.MonitorStop(KioskConfig.IDCardNoDeviceName)
            RemoveHandler PassportScanner.ReceiveEvent, AddressOf PassportScanDataReceived

            RemoveHandler IdcardScanner.eventCardInsertedWithPhoto, AddressOf IdCardScanDataReceived
            RemoveHandler IdcardScanner.eventCardRemoved, AddressOf IdCardRemoved
            RemoveHandler IdcardScanner.eventPhotoProgress, AddressOf readPhotoProgress

        Catch ex As Exception
            InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_OpenForm, "Exception ปิดหน้าจอ " & ex.Message & vbNewLine & ex.StackTrace, True)
        End Try
    End Sub

    Private Sub TimerTimeOut_Tick(sender As Object, e As EventArgs) Handles TimerTimeOut.Tick
        'TimeOut = TimeOut - 1
        TimerTimeOut.Enabled = False
        If KioskConfig.SelectForm = KioskLockerForm.DepositScanPersonInfo Or KioskConfig.SelectForm = KioskLockerForm.CollectScanPersonInfo Then
            Try
                Application.DoEvents()
                If TimeOutCheckTime.AddSeconds(TimeOut) <= DateTime.Now Then
                    TimerTimeOut.Stop()

                    'ปิดการทำงานเครื่องอ่านบัตรประชาชน
                    IdcardScanner.MonitorStop(KioskConfig.IDCardNoDeviceName)
                    RemoveHandler PassportScanner.ReceiveEvent, AddressOf PassportScanDataReceived

                    'ปิดการทำงานเครื่องอ่านหนังสือเดินทาง
                    RemoveHandler IdcardScanner.eventCardInsertedWithPhoto, AddressOf IdCardScanDataReceived
                    RemoveHandler IdcardScanner.eventCardRemoved, AddressOf IdCardRemoved
                    RemoveHandler IdcardScanner.eventPhotoProgress, AddressOf readPhotoProgress

                    If ServiceID = TransactionType.DepositBelonging Then
                        InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_Timeout, "", False)
                        UpdateDepositStatus(Customer.ServiceTransactionID, DepositTransactionData.TransactionStatus.TimeOut, KioskLockerStep.DepositScanPersonInfo_Timeout)
                    ElseIf ServiceID = TransactionType.CollectBelonging Then
                        InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_Timeout, "", False)
                        UpdateCollectStatus(Collect.CollectTransactionID, CollectTransactionData.TransactionStatus.TimeOut, KioskLockerStep.PickupScanPersonInfo_Timeout)
                    End If

                    frmMain.CloseAllChildForm()
                    Dim f As New frmHome
                    f.MdiParent = frmMain
                    f.Show()
                Else
                    TimerTimeOut.Enabled = True
                End If
            Catch ex As Exception
                Dim _err As String = ex.Message & vbNewLine & ex.StackTrace
                If ServiceID = TransactionType.DepositBelonging Then
                    InsertLogTransactionActivity(Customer.DepositTransNo, "", "", KioskConfig.SelectForm, KioskLockerStep.DepositScanPersonInfo_Timeout, _err, True)
                    UpdateDepositStatus(Customer.ServiceTransactionID, DepositTransactionData.TransactionStatus.TimeOut, KioskLockerStep.DepositScanPersonInfo_Timeout)
                ElseIf ServiceID = TransactionType.CollectBelonging Then
                    InsertLogTransactionActivity(Collect.DepositTransNo, Collect.TransactionNo, "", KioskConfig.SelectForm, KioskLockerStep.PickupScanPersonInfo_Timeout, _err, True)
                    UpdateCollectStatus(Collect.CollectTransactionID, CollectTransactionData.TransactionStatus.TimeOut, KioskLockerStep.PickupScanPersonInfo_Timeout)
                End If

                TimerTimeOut.Enabled = True
            End Try
        End If

    End Sub



#End Region
End Class