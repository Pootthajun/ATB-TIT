﻿Public Class MonitorStock

    Dim Slot As String

    Public Property StockSlot() As Integer
        Get
            Return Slot
        End Get
        Set(ByVal value As Integer)
            Slot = value
            lblHead.Text = "Dispenser Slot " & Slot
        End Set
    End Property


End Class
