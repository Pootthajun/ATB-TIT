﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CashInConnectDevice1 = New CashIn.CashInConnectDevice()
        Me.CashOutConnectDevice1 = New CashOut.CashOutConnectDevice()
        Me.CoinOutConnectDevice1 = New CoinOut.CoinOutConnectDevice()
        Me.CoinInConnectDevice1 = New CoinIn.CoinInConnectDevice()
        Me.CashOutConnectDevice2 = New CashOut.CashOutConnectDevice()
        Me.SuspendLayout()
        '
        'CashInConnectDevice1
        '
        Me.CashInConnectDevice1.BackColor = System.Drawing.Color.White
        Me.CashInConnectDevice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CashInConnectDevice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CashInConnectDevice1.Location = New System.Drawing.Point(13, 13)
        Me.CashInConnectDevice1.Margin = New System.Windows.Forms.Padding(4)
        Me.CashInConnectDevice1.Name = "CashInConnectDevice1"
        Me.CashInConnectDevice1.Size = New System.Drawing.Size(320, 160)
        Me.CashInConnectDevice1.TabIndex = 0
        '
        'CashOutConnectDevice1
        '
        Me.CashOutConnectDevice1.BackColor = System.Drawing.Color.White
        Me.CashOutConnectDevice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CashOutConnectDevice1.CashID = 1
        Me.CashOutConnectDevice1.CashType = 100
        Me.CashOutConnectDevice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CashOutConnectDevice1.Location = New System.Drawing.Point(669, 13)
        Me.CashOutConnectDevice1.Margin = New System.Windows.Forms.Padding(4)
        Me.CashOutConnectDevice1.Name = "CashOutConnectDevice1"
        Me.CashOutConnectDevice1.Size = New System.Drawing.Size(320, 160)
        Me.CashOutConnectDevice1.TabIndex = 1
        '
        'CoinOutConnectDevice1
        '
        Me.CoinOutConnectDevice1.BackColor = System.Drawing.Color.White
        Me.CoinOutConnectDevice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CoinOutConnectDevice1.CoinID = 2
        Me.CoinOutConnectDevice1.CoinType = 1
        Me.CoinOutConnectDevice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CoinOutConnectDevice1.Location = New System.Drawing.Point(341, 181)
        Me.CoinOutConnectDevice1.Margin = New System.Windows.Forms.Padding(4)
        Me.CoinOutConnectDevice1.Name = "CoinOutConnectDevice1"
        Me.CoinOutConnectDevice1.Size = New System.Drawing.Size(320, 160)
        Me.CoinOutConnectDevice1.TabIndex = 2
        '
        'CoinInConnectDevice1
        '
        Me.CoinInConnectDevice1.BackColor = System.Drawing.Color.White
        Me.CoinInConnectDevice1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CoinInConnectDevice1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CoinInConnectDevice1.Location = New System.Drawing.Point(13, 181)
        Me.CoinInConnectDevice1.Margin = New System.Windows.Forms.Padding(4)
        Me.CoinInConnectDevice1.Name = "CoinInConnectDevice1"
        Me.CoinInConnectDevice1.Size = New System.Drawing.Size(320, 160)
        Me.CoinInConnectDevice1.TabIndex = 3
        '
        'CashOutConnectDevice2
        '
        Me.CashOutConnectDevice2.BackColor = System.Drawing.Color.White
        Me.CashOutConnectDevice2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CashOutConnectDevice2.CashID = 3
        Me.CashOutConnectDevice2.CashType = 50
        Me.CashOutConnectDevice2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.CashOutConnectDevice2.Location = New System.Drawing.Point(341, 13)
        Me.CashOutConnectDevice2.Margin = New System.Windows.Forms.Padding(4)
        Me.CashOutConnectDevice2.Name = "CashOutConnectDevice2"
        Me.CashOutConnectDevice2.Size = New System.Drawing.Size(320, 160)
        Me.CashOutConnectDevice2.TabIndex = 4
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1057, 586)
        Me.Controls.Add(Me.CashOutConnectDevice2)
        Me.Controls.Add(Me.CoinInConnectDevice1)
        Me.Controls.Add(Me.CoinOutConnectDevice1)
        Me.Controls.Add(Me.CashOutConnectDevice1)
        Me.Controls.Add(Me.CashInConnectDevice1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents CashInConnectDevice1 As CashIn.CashInConnectDevice
    Friend WithEvents CashOutConnectDevice1 As CashOut.CashOutConnectDevice
    Friend WithEvents CoinOutConnectDevice1 As CoinOut.CoinOutConnectDevice
    Friend WithEvents CoinInConnectDevice1 As CoinIn.CoinInConnectDevice
    Friend WithEvents CashOutConnectDevice2 As CashOut.CashOutConnectDevice
End Class
